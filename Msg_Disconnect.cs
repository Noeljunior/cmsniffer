﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CMSniffer
{
    public class Msg_Disconnect : Message
    {
        /* Message Body */
        public ushort resp;

        /* Connect Request Command ID */
        private static string REQUEST_CMD = "7";
        private static string RESPONSE_CMD = "8";

        /* Constructors */
        public Msg_Disconnect(Trans_Hd trans_Hd, MessageType Type)
            : base(trans_Hd, Type)
        {
            listViewTitle = "Disconnect " + (Type == MessageType.Request ? "Request" : "Response");
        }


        public override void fillByByteArray(byte[] raw)
        {
            if (Type == MessageType.Response) /* Response */
            {
                resp = (ushort)(raw[0] << 8 | raw[1]);

                /* Pre-compute ListView Item */
                ListViewItem lvi = new ListViewItem(resp == 1 ? "succeed" : "failed");
                listViewItems.Add(lvi);

            }
            else /* Request */
            {

            }
        }

        public override void setToListView(System.Windows.Forms.ListView listview)
        {
            /* Clear previous ListView */
            listview.Clear();

            /* Set new columns' names */
            if (Type == MessageType.Response) /* Response */
            {
                listview.Columns.Add("Response", 100, HorizontalAlignment.Left);
            }
            else /* Request */
            {

            }

            /* Add new items to the ListView */
            foreach (ListViewItem lvi in listViewItems)
                listview.Items.Add(lvi);
        }

        public override string ToString()
        {
            string output = "";

            if (Type == MessageType.Request)
            {
                output = HeaderToString("DISCONNECT");
            }
            else
            {
                output = HeaderToString("DISCONNECT") +
                String.Format(" {0}", resp);
            }

            return output;
        }

        public static byte[] Request()
        {
            byte[] cmd = ThreadCOM.DecStringToByte(REQUEST_CMD);

            byte[] connect_req = { cmd[1],          cmd[0] };    // CMD

            return connect_req;
        }
    }
}
