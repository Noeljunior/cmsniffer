﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CMSniffer
{
    public class Msg_Generic : Message
    {
        /* Message Body */
        public ushort[] body;
        public string bodyhex;

        /* Connect Request Command ID */
        private static string REQUEST_CMD = "-1";
        private static string RESPONSE_CMD = "-1";

        /* Constructors */
        public Msg_Generic(Trans_Hd trans_Hd, MessageType Type)
            : base(trans_Hd, Type)
        {
            listViewTitle = "Unkown " + (Type == MessageType.Request ? "Request" : "Response");
        }


        public override void fillByByteArray(byte[] raw)
        {
            body = new ushort[raw.Length / 2];
            bodyhex = "";

            /* Parse Message Body */
            for (int i = 0; i < raw.Length; i += 2)
            {
                body[i / 2] = (ushort)(raw[i] << 8 | raw[i + 1]);

                bodyhex += String.Format("{0,4:X4} ", body[i / 2]);
            }

            bodyhex = bodyhex.Trim();

            /* Pre-compute ListView Item */
            ListViewItem lvi = new ListViewItem(bodyhex);
            listViewItems.Add(lvi);
        }

        public override void setToListView(System.Windows.Forms.ListView listview)
        {
            /* Clear previous ListView */
            listview.Clear();

            /* Set new columns' names */
            listview.Columns.Add("Value", -1, HorizontalAlignment.Left);

            /* Add new items to the ListView */
            foreach (ListViewItem lvi in listViewItems)
                listview.Items.Add(lvi);

            listview.Columns[0].Width = -1;
        }

        public override string ToString()
        {
            return HeaderToString("UNKNOW") +
                String.Format(" {0}", bodyhex);
        }


    }
}
