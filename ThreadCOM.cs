﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO.Ports;

namespace CMSniffer
{
    public class ThreadCOM
    {
        /* MainClass reference */
        private MainClass mainClass;

        /* SerialPort object */
        private SerialPort serialPort;

        /* Parser thread */
        private Thread parserThread;

        /* Queue objects and semaphores */
        private Queue<int> inbuffer = new Queue<int>();
        private readonly object parserLock = new object();


        public ThreadCOM(MainClass mainClass)
        {
            this.mainClass = mainClass;

            /* New SerialPort object */
            serialPort = new SerialPort();
            serialPort.DataReceived += new SerialDataReceivedEventHandler(SerialReceived);

            /* New parser thread */
            parserThread = new Thread(new ThreadStart(parseWorker));
            parserThread.Start();
        }

        public bool isOpen()
        {
            return serialPort.IsOpen;
        }

        public bool OpenSerialPort(string port)
        {
            try
            {
                serialPort.PortName = port;
                serialPort.BaudRate = 38400;

                serialPort.DataBits = 8;
                serialPort.DiscardNull = false;
                serialPort.StopBits = StopBits.One;
                serialPort.Parity = Parity.None;
                serialPort.ReadTimeout = 2000;
                serialPort.DtrEnable = true;
                serialPort.RtsEnable = true;

                serialPort.Handshake = Handshake.RequestToSend;

                serialPort.ReadBufferSize = 16384;

                serialPort.Open();
            }
            catch  {
                mainClass.mainGUI.GUIUpdate(MainClass.Call.OpenPortError, "error while openning port");
                CloseSerialPort();
            }

            return serialPort.IsOpen;
        }

        /* Closes serial port and terminate threads */
        public void Shutdown()
        {
            CloseSerialPort();

            parserThread.Abort();  
        }
        
        /* Closes the serial port and clears buffer */
        public void CloseSerialPort()
        {
            Thread.Sleep(250);
            if (serialPort.IsOpen)
            {
                serialPort.Close();
            }

            lock (parserLock)
                inbuffer.Clear();

            mainClass.mainGUI.GUIUpdate(MainClass.Call.ClosedPort, null);
        }

        /* Sends a byte array of one request */
        public void sendRequest(byte[] request)
        {
            if (!serialPort.IsOpen)
            {
                mainClass.mainGUI.GUIUpdate(MainClass.Call.OpenPortError, "error while writing to port: port has been closed");
                CloseSerialPort();
                return;
            }

            /* Build Transfer Header byte stream */
            byte[] btrans_hd = new byte[6];

            /* Destination ID */
            btrans_hd[2] = (byte)((mainClass.destinationID & 0xFF00) >> 8);
            btrans_hd[3] = (byte)(mainClass.destinationID & 0x00FF);

            /* Source ID */
            btrans_hd[4] = (byte)((mainClass.sourceID & 0xFF00) >> 8);
            btrans_hd[5] = (byte)(mainClass.sourceID & 0x00FF);

            /* Cacatenate, calculate size and add sync char */
            byte[] output = new byte[1 + btrans_hd.Length + request.Length];
            btrans_hd[0] = (byte)(((btrans_hd.Length + request.Length) & 0xFF00) >> 8);
            btrans_hd[1] = (byte)(btrans_hd.Length + request.Length);

            /* Sync char */
            output[0] = 0x1B;

            /* Copy transfer header and command */
            Array.Copy(btrans_hd, 0, output, 1, 6);
            Array.Copy(request, 0, output, 7, request.Length);

            /* Change the endianess to Big-Endian */
            for (int i = 1; i < output.Length; i += 2)
            {
                byte u8 = output[i];
                output[i] = output[i + 1];
                output[i + 1] = u8;
            }

            /* Add escape char */
            /* Calculate how many escapes chars will be needed */
            int escaptesCount = 0;
            for (int i = 1; i < output.Length; i++)
                if (output[i] == 0x1B)
                    escaptesCount++;
            /* Allocate more space  */
            byte[] escapedoutput = new byte[output.Length + escaptesCount];
            escapedoutput[0] = output[0];
            int outputi = 1;
            for (int i = 1; i < escapedoutput.Length; i++)
            {
                if (output[i] == 0x1B) /* Add escape */
                    escapedoutput[i] = 0xFF;
                else
                    escapedoutput[i] = output[outputi++];
            }
            output = escapedoutput;

            /* Write byte array to output stream */
            serialPort.Write(output, 0, output.Length);

            /* Add message to ArrayList of messages */
            byte[] toParse = new byte[btrans_hd.Length + request.Length];
            Array.Copy(btrans_hd, 0, toParse, 0, 6);
            Array.Copy(request, 0, toParse, 6, request.Length);
            ParseMessage(toParse);
        }

        /* Event handler of received bytes from serial port */
        private void SerialReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                while (serialPort.BytesToRead > 0)
                {
                    inbuffer.Enqueue(serialPort.ReadByte());

                    /* DEBUG */
                    /*int read = serialPort.ReadByte();
                    String lines = String.Format("{0,2:X2} ", read);
                    System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\debugRAW.txt", true);
                    if (read == 27)
                        file.WriteLine("");
                    file.Write(lines);
                    file.Close();
                    inbuffer.Enqueue(read);/**/
                    
                }
                lock (parserLock)
                    Monitor.Pulse(parserLock);
            }
            catch 
            {
                CloseSerialPort();
                mainClass.mainGUI.GUIUpdate(MainClass.Call.DisconnectSucceed, null);
                mainClass.mainGUI.GUIUpdate(MainClass.Call.ClosedPort, null);

            }
        }


        /* Message Parser Thread */
        public void parseWorker()
        { 

            int lenght;                 /* lenght of message */
            int rawoffset;              /* offset to write on raw message */
            byte[] raw;                    /* raw message - little-endianed and 0xFF escaped */
            byte[] u16 = new byte[2];      /* two bytes temporary variable */
            byte u8,                       /* one byte temporary variable */
                 u8p;                      /* one byte temporary variable - previous read */

            while (true)
            {
                /* Read sync char */
                if ((u8 = getByte()) == 0x1B)    /* success */
                {
                    Console.WriteLine("Sync char: starting new message...");
                }
                else                                /* Sync char error */
                {
                    Console.WriteLine("SYNC ERROR: Not expected char '" + u8.ToString() + "'");
                    continue;
                }

            RESYNC:

                /* Read lenght */
                u16[0] = getByte();         /* Read 2 bytes - big-endianed */
                u16[1] = getByte();
                lenght = u16[0] | u16[1] << 8;      /* Convert them to a single little-endian */

                raw = new byte[lenght];             /* Allocate memory to raw message array */

                raw[0] = u16[1];                    /* Store most significant bits of lenght */
                raw[1] = u16[0];                    /* Store least significant bits of lenght */

                /* Read at least lenght bytes */
                lenght -= 2;        /* Decrement lenght's block size */
                rawoffset = 2;
                
                u8p = 0;

                while (lenght > 0)
                {
                    /*u8 = peekByte();
                    if (u8 == 0xFF && raw[rawoffset - 1] == 0x1B)
                    {
                        getByte();
                        continue;
                    }
                    else if (u8 != 0xFF && raw[rawoffset - 1] == 0x1B)
                    {
                        //Console.WriteLine("RESYNC detected");
                        //goto RESYNC;
                    }*/

                    u8 = getByte();

                    if (u8 == 0xFF && u8p == 0x1B) /* Escape char detected */
                    {
                        u8p = u8;
                        Console.WriteLine("Escape char detected");
                        continue;
                    }
                    else if (u8 != 0xFF && u8p == 0x1B) /* Resync char detected */
                    {
                        Console.WriteLine("RESYNC detected");
                        goto RESYNC;
                    }

                    u8p = u8;

                    lenght--;
                    raw[rawoffset++] = u8; /* Add last read character to message array */


                }

                /* Convert big-endian array to little-endian */
                for (int i = 2; i < raw.Length; i += 2)
                {
                    u8 = raw[i];
                    raw[i] = raw[i + 1];
                    raw[i + 1] = u8;
                }

                ParseMessage(raw);

            }
        }

        /* Get one inbuffer byte */
        private byte getByte()
        {
            byte outbyte;
            lock (parserLock)
            {
                while (inbuffer.Count <= 0)
                {
                    Monitor.Wait(parserLock);
                }
                outbyte = (byte)inbuffer.Dequeue();
            }

            return outbyte;
        }

        /* Peek one inbuffer byte */
        private byte peekByte()
        {
            byte outbyte;
            lock (parserLock)
            {
                while (inbuffer.Count <= 0)
                {
                    Monitor.Wait(parserLock);
                }
                outbyte = (byte)inbuffer.Peek();
            }

            return outbyte;
        }

        /* Parser new raw message */
        private void ParseMessage(byte[] rawmsg)
        {
            /* DEBUG */
            /*String lines = "";
            for (int i = 0; i < rawmsg.Length; i++) {
                lines += String.Format("{0,2:X2} ", rawmsg[i]);
            }
            System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\debug.txt", true);
            file.WriteLine(lines.Trim());
            file.Close();*/

            /* Create new received message's header */
            Message.Trans_Hd trans_Hd;
            trans_Hd = new Message.Trans_Hd((ushort)(rawmsg[0] << 8 | rawmsg[1]),
                                            (ushort)(rawmsg[2] << 8 | rawmsg[3]),
                                            (ushort)(rawmsg[4] << 8 | rawmsg[5]),
                                            (ushort)(rawmsg[6] << 8 | rawmsg[7]));

            /* Find out which message is beeing received */
            Message msg;
            switch (trans_Hd.command)
            {
                /* Connect message */
                case 1:     /* Request */
                    msg = new Msg_Connect(trans_Hd, Message.MessageType.Request);
                    break;
                case 2:     /* Response */
                    msg = new Msg_Connect(trans_Hd, Message.MessageType.Response);
                    break;

                /* Notifications */
                case 5: 
                    msg = new Msg_Notifications(trans_Hd, Message.MessageType.Notification);
                    break;
                case 6:
                    msg = new Msg_Notifications(trans_Hd, Message.MessageType.Notification);
                    break;

                /* Disconnect */
                case 7:    /* Request */
                    msg = new Msg_Disconnect(trans_Hd, Message.MessageType.Request);
                    break;
                case 8:    /* Response */
                    msg = new Msg_Disconnect(trans_Hd, Message.MessageType.Response);
                    break;

                /* Par List */
                case 11:    /* Request */
                    msg = new Msg_Par_List(trans_Hd, Message.MessageType.Request);
                    break;
                case 12:    /* Response */
                    msg = new Msg_Par_List(trans_Hd, Message.MessageType.Response);
                    break;

                /* Single Tune */
                case 15:    /* Request */
                    msg = new Msg_Tune(trans_Hd, Message.MessageType.Request);
                    break;
                case 16:    /* Response */
                    msg = new Msg_Tune(trans_Hd, Message.MessageType.Response);
                    break;
                //TODO if é resposta de Single Tune

                default:
                    if (mainClass.checkForKnownTune(trans_Hd.command)) /* Check a known tune mbpHd */
                    {
                        msg = new Msg_Data(trans_Hd, Message.MessageType.Data);
                        break;
                    }

                    msg = new Msg_Generic(trans_Hd,
                        trans_Hd.command % 2 == 0 ? Message.MessageType.Response : Message.MessageType.Request);
                    break;
            }

            /* Extract message body : excluding command ID */
            byte[] rawmsgbody = new byte[rawmsg.Length - 8];
            Array.Copy(rawmsg, 8, rawmsgbody, 0, rawmsg.Length - 8);

            /* Fill new message object with new received message's info */
            msg.fillByByteArray(rawmsgbody);

            /* Add new message to the messages' ArrayList */
            mainClass.addMessage(msg);
        }

        /* Decimal-string to 2bytes */
        public static byte[] DecStringToByte(string input)
        {
            byte[] output = new byte[2];
            ushort uoutput = ushort.Parse(input.Trim(), System.Globalization.NumberStyles.Integer);

            output[0] = (byte)(uoutput & 0x00FF);
            output[1] = (byte)((uoutput & 0xFF00) >> 8);

            //Console.WriteLine(output[1] + ":" + output[0]);

            return output;
        }

        /* Convert bytes to string */
        public static string binToString(byte[] input)
        {
            String output = "";

            for (int i = 0; i < input.Length; i += 2)
            {
                try
                {
                    output = output + charset[input[i] << 8 | input[i + 1]];
                }
                catch
                {
                    output = output + "?";
                }

            }
            return output.Trim();
        }

        /* Charset Table */
        private static char[] charset = {
        Convert.ToChar(0),  Convert.ToChar(1),  Convert.ToChar(2),  Convert.ToChar(3),  Convert.ToChar(4),  Convert.ToChar(5),  Convert.ToChar(6),  Convert.ToChar(7),  Convert.ToChar(8),  Convert.ToChar(9),
        Convert.ToChar(10), Convert.ToChar(11), Convert.ToChar(12), Convert.ToChar(13), Convert.ToChar(14), Convert.ToChar(15), Convert.ToChar(16), Convert.ToChar(17), Convert.ToChar(18), Convert.ToChar(19),
        Convert.ToChar(20), Convert.ToChar(21), Convert.ToChar(22), Convert.ToChar(23), Convert.ToChar(24), Convert.ToChar(25), Convert.ToChar(26), Convert.ToChar(27), Convert.ToChar(28), Convert.ToChar(29),
        Convert.ToChar(30), Convert.ToChar(31), Convert.ToChar(32), Convert.ToChar(33), Convert.ToChar(34), Convert.ToChar(35), Convert.ToChar(36), Convert.ToChar(37), Convert.ToChar(38), Convert.ToChar(39),
        Convert.ToChar(40), Convert.ToChar(41), Convert.ToChar(42), Convert.ToChar(43), Convert.ToChar(44), Convert.ToChar(45), Convert.ToChar(46), Convert.ToChar(47), Convert.ToChar(48), Convert.ToChar(49),
        Convert.ToChar(50), Convert.ToChar(51), Convert.ToChar(52), Convert.ToChar(53), Convert.ToChar(54), Convert.ToChar(55), Convert.ToChar(56), Convert.ToChar(57), Convert.ToChar(58), Convert.ToChar(59),
        Convert.ToChar(60), Convert.ToChar(61), Convert.ToChar(62), Convert.ToChar(63), Convert.ToChar(64), Convert.ToChar(65), Convert.ToChar(66), Convert.ToChar(67), Convert.ToChar(68), Convert.ToChar(69),
        Convert.ToChar(70), Convert.ToChar(71), Convert.ToChar(72), Convert.ToChar(73), Convert.ToChar(74), Convert.ToChar(75), Convert.ToChar(76), Convert.ToChar(77), Convert.ToChar(78), Convert.ToChar(79),
        Convert.ToChar(80), Convert.ToChar(81), Convert.ToChar(82), Convert.ToChar(83), Convert.ToChar(84), Convert.ToChar(85), Convert.ToChar(86), Convert.ToChar(87), Convert.ToChar(88), Convert.ToChar(89),
        Convert.ToChar(90), Convert.ToChar(91), Convert.ToChar(92), Convert.ToChar(93), Convert.ToChar(94), Convert.ToChar(95), Convert.ToChar(96), Convert.ToChar(97), Convert.ToChar(98), Convert.ToChar(99),
        Convert.ToChar(100),Convert.ToChar(101),Convert.ToChar(102),Convert.ToChar(103),Convert.ToChar(104),Convert.ToChar(105),Convert.ToChar(106),Convert.ToChar(107),Convert.ToChar(108),Convert.ToChar(109),
        Convert.ToChar(110),Convert.ToChar(111),Convert.ToChar(112),Convert.ToChar(113),Convert.ToChar(114),Convert.ToChar(115),Convert.ToChar(116),Convert.ToChar(117),Convert.ToChar(118),Convert.ToChar(119),
        Convert.ToChar(120),Convert.ToChar(121),Convert.ToChar(122),Convert.ToChar(123),Convert.ToChar(124),Convert.ToChar(125),Convert.ToChar(126),
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',' ', ' ', ' ', ' ', ' ', ' ',
        Convert.ToChar(96), Convert.ToChar(94),  ' ', Convert.ToChar(126), Convert.ToChar(151), Convert.ToChar(150), Convert.ToChar(156), ' ', ' ', Convert.ToChar(17), Convert.ToChar(248), 
        Convert.ToChar(128), Convert.ToChar(135), Convert.ToChar(165), Convert.ToChar(164), Convert.ToChar(173), ' ', ' ', Convert.ToChar(156), Convert.ToChar(157), Convert.ToChar(21),  Convert.ToChar(159),
        Convert.ToChar(155), Convert.ToChar(131), Convert.ToChar(136), Convert.ToChar(147), Convert.ToChar(150), Convert.ToChar(160), Convert.ToChar(130), Convert.ToChar(162), Convert.ToChar(163), 
        Convert.ToChar(133), Convert.ToChar(138), Convert.ToChar(149),  Convert.ToChar(151), Convert.ToChar(132), Convert.ToChar(137), Convert.ToChar(148), Convert.ToChar(129), Convert.ToChar(143),
        Convert.ToChar(173), Convert.ToChar(237), Convert.ToChar(146), Convert.ToChar(134), Convert.ToChar(161), Convert.ToChar(237),          
        Convert.ToChar(145), Convert.ToChar(142), Convert.ToChar(141), Convert.ToChar(153), Convert.ToChar(154), Convert.ToChar(144), Convert.ToChar(139), Convert.ToChar(225),
        Convert.ToChar(147), Convert.ToChar(160), Convert.ToChar(65),  Convert.ToChar(97), Convert.ToChar(68), ' ',Convert.ToChar(161),   Convert.ToChar(141),  Convert.ToChar(162),  Convert.ToChar(149), 
        Convert.ToChar(79), Convert.ToChar(111),Convert.ToChar(83), Convert.ToChar(115), Convert.ToChar(163), Convert.ToChar(152), Convert.ToChar(152), ' ',
        ' ', ' ', ' ', ' ',  ' ', Convert.ToChar(45), Convert.ToChar(172),   Convert.ToChar(171), Convert.ToChar(97),  Convert.ToChar(111),   Convert.ToChar(174),   Convert.ToChar(219),   Convert.ToChar(175),  Convert.ToChar(18),   ' ',
        ' ', ' ', ' ', ' ', ' ', ' ',  ' ',  ' ',  ' ',' ', ' ',  ' ',' ', Convert.ToChar(179),  Convert.ToChar(196),  Convert.ToChar(197), Convert.ToChar(192),   Convert.ToChar(217), 
        Convert.ToChar(218),   Convert.ToChar(191),  Convert.ToChar(195),  Convert.ToChar(180),  Convert.ToChar(194), Convert.ToChar(193),   Convert.ToChar(24),   Convert.ToChar(25), 
        Convert.ToChar(253),   Convert.ToChar(53),  Convert.ToChar(67), Convert.ToChar(70), Convert.ToChar(108),  Convert.ToChar(50),  Convert.ToChar(72),  Convert.ToChar(51), 
        ' ', ' ', Convert.ToChar(17),  Convert.ToChar(16),  Convert.ToChar(30), Convert.ToChar(31), ' ',    ' ', ' ', ' ', ' ', ' ', ' ',  ' ', ' ',
        /**/
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',' ', ' ', ' ',' ', ' ', ' ',' ', ' ', ' ', ' ', ' ',' ', ' ',  ' ',  ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', Convert.ToChar(42),   
        Convert.ToChar(42),  ' ', ' ', ' ', Convert.ToChar(40), Convert.ToChar(41), Convert.ToChar(47), ' ', ' ', Convert.ToChar(63), ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', Convert.ToChar(48), Convert.ToChar(49), 
        Convert.ToChar(50), Convert.ToChar(51), Convert.ToChar(52), Convert.ToChar(53), Convert.ToChar(54), Convert.ToChar(55), Convert.ToChar(56), Convert.ToChar(57), Convert.ToChar(47), ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', Convert.ToChar(32), Convert.ToChar(33), Convert.ToChar(34), 
        Convert.ToChar(35), Convert.ToChar(36), Convert.ToChar(37), Convert.ToChar(38), Convert.ToChar(39), Convert.ToChar(40), Convert.ToChar(41), Convert.ToChar(42), Convert.ToChar(43), Convert.ToChar(44), Convert.ToChar(45), 
        Convert.ToChar(46), Convert.ToChar(47), Convert.ToChar(48), Convert.ToChar(49), Convert.ToChar(50), Convert.ToChar(51), Convert.ToChar(52), Convert.ToChar(53), Convert.ToChar(54), Convert.ToChar(55), Convert.ToChar(56), 
        Convert.ToChar(57), Convert.ToChar(58), Convert.ToChar(59), Convert.ToChar(60), Convert.ToChar(61), Convert.ToChar(62), Convert.ToChar(63), Convert.ToChar(64), Convert.ToChar(65), Convert.ToChar(66), Convert.ToChar(67), 
        Convert.ToChar(68), Convert.ToChar(69), Convert.ToChar(70), Convert.ToChar(71), Convert.ToChar(72), Convert.ToChar(73), Convert.ToChar(74), Convert.ToChar(75), Convert.ToChar(76), Convert.ToChar(77), Convert.ToChar(78), 
        Convert.ToChar(79), Convert.ToChar(80), Convert.ToChar(81), Convert.ToChar(82), Convert.ToChar(83), Convert.ToChar(84), Convert.ToChar(85), Convert.ToChar(86), Convert.ToChar(87), Convert.ToChar(88), Convert.ToChar(89), 
        Convert.ToChar(90), Convert.ToChar(91), Convert.ToChar(92), Convert.ToChar(93), Convert.ToChar(94), Convert.ToChar(95), Convert.ToChar(96), Convert.ToChar(97), Convert.ToChar(98), Convert.ToChar(99), Convert.ToChar(100),
        Convert.ToChar(101),Convert.ToChar(102),Convert.ToChar(103),Convert.ToChar(104),Convert.ToChar(105),Convert.ToChar(106),Convert.ToChar(107),Convert.ToChar(108),Convert.ToChar(109), Convert.ToChar(110),Convert.ToChar(111),
        Convert.ToChar(112),Convert.ToChar(113),Convert.ToChar(114),Convert.ToChar(115),Convert.ToChar(116),Convert.ToChar(117),Convert.ToChar(118),Convert.ToChar(119), Convert.ToChar(120),Convert.ToChar(121),Convert.ToChar(122),
        Convert.ToChar(123),Convert.ToChar(124),Convert.ToChar(125),Convert.ToChar(126),Convert.ToChar(127), ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', Convert.ToChar(96), Convert.ToChar(94), ' ', Convert.ToChar(126), Convert.ToChar(151), Convert.ToChar(150), Convert.ToChar(156), 
        ' ',  ' ', Convert.ToChar(17), Convert.ToChar(248), Convert.ToChar(128), Convert.ToChar(135), Convert.ToChar(165), Convert.ToChar(164), Convert.ToChar(173), ' ', ' ', Convert.ToChar(156), Convert.ToChar(157), Convert.ToChar(21),   
        Convert.ToChar(159), Convert.ToChar(155), Convert.ToChar(131), Convert.ToChar(136), Convert.ToChar(147), Convert.ToChar(150), Convert.ToChar(160), Convert.ToChar(130), Convert.ToChar(162), Convert.ToChar(163), Convert.ToChar(133), 
        Convert.ToChar(138), Convert.ToChar(149), Convert.ToChar(151), Convert.ToChar(132), Convert.ToChar(137), Convert.ToChar(148), Convert.ToChar(129), Convert.ToChar(143), Convert.ToChar(173), Convert.ToChar(237), Convert.ToChar(146),   
        Convert.ToChar(134), Convert.ToChar(161), Convert.ToChar(237), Convert.ToChar(145), Convert.ToChar(142), Convert.ToChar(141), Convert.ToChar(153), Convert.ToChar(154), Convert.ToChar(144), Convert.ToChar(139), Convert.ToChar(225),
        Convert.ToChar(147), Convert.ToChar(160), Convert.ToChar(65), Convert.ToChar(97), Convert.ToChar(68), ' ', Convert.ToChar(161), Convert.ToChar(141), Convert.ToChar(162), Convert.ToChar(149), Convert.ToChar(79), Convert.ToChar(111), 
        Convert.ToChar(83), Convert.ToChar(115), Convert.ToChar(163), Convert.ToChar(152), Convert.ToChar(152), ' ', ' ', ' ', ' ', ' ', ' ', Convert.ToChar(45), Convert.ToChar(172), Convert.ToChar(171), Convert.ToChar(97), Convert.ToChar(111), 
        Convert.ToChar(174), Convert.ToChar(219), Convert.ToChar(175), Convert.ToChar(18), ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', Convert.ToChar(179), Convert.ToChar(196), Convert.ToChar(197), Convert.ToChar(192), 
        Convert.ToChar(217), Convert.ToChar(218), Convert.ToChar(191), Convert.ToChar(195), Convert.ToChar(180), Convert.ToChar(194), Convert.ToChar(193), Convert.ToChar(24), Convert.ToChar(25), Convert.ToChar(253), Convert.ToChar(53), Convert.ToChar(67), 
        Convert.ToChar(70), Convert.ToChar(108), Convert.ToChar(50), Convert.ToChar(72), Convert.ToChar(51), ' ', ' ', Convert.ToChar(17), Convert.ToChar(16), Convert.ToChar(30), Convert.ToChar(31), Convert.ToChar(0) };
        
       

    }
}
