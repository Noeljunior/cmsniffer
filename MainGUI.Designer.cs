﻿namespace CMSniffer
{
    partial class MainGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainGUI));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btConnect = new System.Windows.Forms.ToolStripSplitButton();
            this.cbSerialPort = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.sourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbSourceID = new System.Windows.Forms.ToolStripTextBox();
            this.destinationIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbDestinationID = new System.Windows.Forms.ToolStripTextBox();
            this.btConsole = new System.Windows.Forms.ToolStripButton();
            this.btListChannels = new System.Windows.Forms.ToolStripButton();
            this.btSingleTune = new System.Windows.Forms.ToolStripSplitButton();
            this.cbSelectedTunes = new System.Windows.Forms.ToolStripComboBox();
            this.tsmiClearTunes = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tuneIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbTuneID = new System.Windows.Forms.ToolStripTextBox();
            this.recordIDToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tbRecordID = new System.Windows.Forms.ToolStripTextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lbStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lbSerialStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lbCMSStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.scMessagesSplit = new System.Windows.Forms.SplitContainer();
            this.lvMessagesList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvMessageInfo = new System.Windows.Forms.ListView();
            this.cmsParList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiAddTune = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSingleTune = new System.Windows.Forms.ToolStripMenuItem();
            this.scMainSplit = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btConsoleSend = new System.Windows.Forms.Button();
            this.cbConsoleWriter = new System.Windows.Forms.ComboBox();
            this.tbConsoleViewer = new System.Windows.Forms.TextBox();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMessagesSplit)).BeginInit();
            this.scMessagesSplit.Panel1.SuspendLayout();
            this.scMessagesSplit.Panel2.SuspendLayout();
            this.scMessagesSplit.SuspendLayout();
            this.cmsParList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMainSplit)).BeginInit();
            this.scMainSplit.Panel1.SuspendLayout();
            this.scMainSplit.Panel2.SuspendLayout();
            this.scMainSplit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btConnect,
            this.btConsole,
            this.btListChannels,
            this.btSingleTune});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(821, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btConnect
            // 
            this.btConnect.AutoSize = false;
            this.btConnect.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbSerialPort,
            this.toolStripSeparator5,
            this.sourceToolStripMenuItem,
            this.destinationIDToolStripMenuItem});
            this.btConnect.Image = ((System.Drawing.Image)(resources.GetObject("btConnect.Image")));
            this.btConnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btConnect.Name = "btConnect";
            this.btConnect.Size = new System.Drawing.Size(105, 22);
            this.btConnect.Text = "Connect";
            this.btConnect.ToolTipText = "Opens the selected serial port and tries to stabilish a new connection.\r\nIf the p" +
    "or is already opened it will try to close the connection and close the serial po" +
    "rt.";
            this.btConnect.ButtonClick += new System.EventHandler(this.btConnect_Click);
            // 
            // cbSerialPort
            // 
            this.cbSerialPort.Name = "cbSerialPort";
            this.cbSerialPort.Size = new System.Drawing.Size(121, 23);
            this.cbSerialPort.ToolTipText = "Specify serial port number.";
            this.cbSerialPort.DropDown += new System.EventHandler(this.cbSerialPort_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(178, 6);
            // 
            // sourceToolStripMenuItem
            // 
            this.sourceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbSourceID});
            this.sourceToolStripMenuItem.Name = "sourceToolStripMenuItem";
            this.sourceToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.sourceToolStripMenuItem.Text = "Source ID";
            // 
            // tbSourceID
            // 
            this.tbSourceID.Name = "tbSourceID";
            this.tbSourceID.Size = new System.Drawing.Size(100, 23);
            this.tbSourceID.Text = "10";
            // 
            // destinationIDToolStripMenuItem
            // 
            this.destinationIDToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbDestinationID});
            this.destinationIDToolStripMenuItem.Name = "destinationIDToolStripMenuItem";
            this.destinationIDToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.destinationIDToolStripMenuItem.Text = "Destination ID";
            // 
            // tbDestinationID
            // 
            this.tbDestinationID.Name = "tbDestinationID";
            this.tbDestinationID.Size = new System.Drawing.Size(100, 23);
            this.tbDestinationID.Text = "32865";
            // 
            // btConsole
            // 
            this.btConsole.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btConsole.Image = ((System.Drawing.Image)(resources.GetObject("btConsole.Image")));
            this.btConsole.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btConsole.Name = "btConsole";
            this.btConsole.Size = new System.Drawing.Size(70, 22);
            this.btConsole.Text = "Console";
            this.btConsole.Click += new System.EventHandler(this.btConsole_Click);
            // 
            // btListChannels
            // 
            this.btListChannels.Enabled = false;
            this.btListChannels.Image = ((System.Drawing.Image)(resources.GetObject("btListChannels.Image")));
            this.btListChannels.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btListChannels.Name = "btListChannels";
            this.btListChannels.Size = new System.Drawing.Size(95, 22);
            this.btListChannels.Text = "List channels";
            this.btListChannels.ToolTipText = "Query the CMS for its suported channels.";
            this.btListChannels.Click += new System.EventHandler(this.btListChannels_Click);
            // 
            // btSingleTune
            // 
            this.btSingleTune.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbSelectedTunes,
            this.tsmiClearTunes,
            this.toolStripSeparator3,
            this.tuneIDToolStripMenuItem,
            this.recordIDToolStripMenuItem1});
            this.btSingleTune.Enabled = false;
            this.btSingleTune.Image = ((System.Drawing.Image)(resources.GetObject("btSingleTune.Image")));
            this.btSingleTune.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSingleTune.Name = "btSingleTune";
            this.btSingleTune.Size = new System.Drawing.Size(101, 22);
            this.btSingleTune.Text = "Single Tune";
            this.btSingleTune.ToolTipText = "Send a single tune request for all of selected channels";
            this.btSingleTune.ButtonClick += new System.EventHandler(this.btSingleTune_Click);
            // 
            // cbSelectedTunes
            // 
            this.cbSelectedTunes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSelectedTunes.MaxDropDownItems = 10;
            this.cbSelectedTunes.Name = "cbSelectedTunes";
            this.cbSelectedTunes.Size = new System.Drawing.Size(121, 23);
            this.cbSelectedTunes.ToolTipText = "List of selected channels to single tune.\r\nClick on one of them to remove it from" +
    " the list.";
            this.cbSelectedTunes.DropDown += new System.EventHandler(this.cbSelectedTunes_Click);
            // 
            // tsmiClearTunes
            // 
            this.tsmiClearTunes.Name = "tsmiClearTunes";
            this.tsmiClearTunes.Size = new System.Drawing.Size(181, 22);
            this.tsmiClearTunes.Text = "Clear selected tuns";
            this.tsmiClearTunes.Click += new System.EventHandler(this.tsmiClearTunes_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(178, 6);
            // 
            // tuneIDToolStripMenuItem
            // 
            this.tuneIDToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbTuneID});
            this.tuneIDToolStripMenuItem.Name = "tuneIDToolStripMenuItem";
            this.tuneIDToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.tuneIDToolStripMenuItem.Text = "Tune ID";
            // 
            // tbTuneID
            // 
            this.tbTuneID.Name = "tbTuneID";
            this.tbTuneID.Size = new System.Drawing.Size(100, 23);
            // 
            // recordIDToolStripMenuItem1
            // 
            this.recordIDToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbRecordID});
            this.recordIDToolStripMenuItem1.Name = "recordIDToolStripMenuItem1";
            this.recordIDToolStripMenuItem1.Size = new System.Drawing.Size(181, 22);
            this.recordIDToolStripMenuItem1.Text = "Record ID";
            // 
            // tbRecordID
            // 
            this.tbRecordID.Name = "tbRecordID";
            this.tbRecordID.Size = new System.Drawing.Size(100, 23);
            this.tbRecordID.Text = "0";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2,
            this.lbStatus,
            this.toolStripStatusLabel5,
            this.toolStripStatusLabel1,
            this.lbSerialStatus,
            this.toolStripStatusLabel3,
            this.lbCMSStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 390);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(821, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel2.Text = " ";
            // 
            // lbStatus
            // 
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(0, 17);
            this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(593, 17);
            this.toolStripStatusLabel5.Spring = true;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(38, 17);
            this.toolStripStatusLabel1.Text = "COM:";
            // 
            // lbSerialStatus
            // 
            this.lbSerialStatus.AutoSize = false;
            this.lbSerialStatus.Name = "lbSerialStatus";
            this.lbSerialStatus.Size = new System.Drawing.Size(50, 17);
            this.lbSerialStatus.Text = "closed";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(35, 17);
            this.toolStripStatusLabel3.Text = "CMS:";
            // 
            // lbCMSStatus
            // 
            this.lbCMSStatus.AutoSize = false;
            this.lbCMSStatus.Name = "lbCMSStatus";
            this.lbCMSStatus.Size = new System.Drawing.Size(80, 17);
            this.lbCMSStatus.Text = "disconnected";
            // 
            // scMessagesSplit
            // 
            this.scMessagesSplit.BackColor = System.Drawing.Color.DarkGray;
            this.scMessagesSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMessagesSplit.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.scMessagesSplit.Location = new System.Drawing.Point(0, 0);
            this.scMessagesSplit.Name = "scMessagesSplit";
            // 
            // scMessagesSplit.Panel1
            // 
            this.scMessagesSplit.Panel1.Controls.Add(this.lvMessagesList);
            this.scMessagesSplit.Panel1MinSize = 200;
            // 
            // scMessagesSplit.Panel2
            // 
            this.scMessagesSplit.Panel2.Controls.Add(this.lvMessageInfo);
            this.scMessagesSplit.Size = new System.Drawing.Size(821, 178);
            this.scMessagesSplit.SplitterDistance = 200;
            this.scMessagesSplit.SplitterWidth = 1;
            this.scMessagesSplit.TabIndex = 3;
            // 
            // lvMessagesList
            // 
            this.lvMessagesList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvMessagesList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lvMessagesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvMessagesList.FullRowSelect = true;
            this.lvMessagesList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvMessagesList.HideSelection = false;
            this.lvMessagesList.Location = new System.Drawing.Point(0, 0);
            this.lvMessagesList.MultiSelect = false;
            this.lvMessagesList.Name = "lvMessagesList";
            this.lvMessagesList.Size = new System.Drawing.Size(200, 178);
            this.lvMessagesList.TabIndex = 0;
            this.lvMessagesList.UseCompatibleStateImageBehavior = false;
            this.lvMessagesList.View = System.Windows.Forms.View.Details;
            this.lvMessagesList.SelectedIndexChanged += new System.EventHandler(this.lvMessagesList_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Message Type";
            this.columnHeader1.Width = 180;
            // 
            // lvMessageInfo
            // 
            this.lvMessageInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvMessageInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvMessageInfo.FullRowSelect = true;
            this.lvMessageInfo.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvMessageInfo.Location = new System.Drawing.Point(0, 0);
            this.lvMessageInfo.MultiSelect = false;
            this.lvMessageInfo.Name = "lvMessageInfo";
            this.lvMessageInfo.Size = new System.Drawing.Size(620, 178);
            this.lvMessageInfo.TabIndex = 0;
            this.lvMessageInfo.UseCompatibleStateImageBehavior = false;
            this.lvMessageInfo.View = System.Windows.Forms.View.Details;
            this.lvMessageInfo.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvMessageInfo_MouseDoubleClick);
            // 
            // cmsParList
            // 
            this.cmsParList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAddTune,
            this.tsmiSingleTune});
            this.cmsParList.Name = "cmsParList";
            this.cmsParList.Size = new System.Drawing.Size(189, 48);
            this.cmsParList.Opening += new System.ComponentModel.CancelEventHandler(this.cmsParList_Opening);
            // 
            // tsmiAddTune
            // 
            this.tsmiAddTune.Name = "tsmiAddTune";
            this.tsmiAddTune.Size = new System.Drawing.Size(188, 22);
            this.tsmiAddTune.Text = "Add to selected tunes";
            this.tsmiAddTune.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // tsmiSingleTune
            // 
            this.tsmiSingleTune.Name = "tsmiSingleTune";
            this.tsmiSingleTune.Size = new System.Drawing.Size(188, 22);
            this.tsmiSingleTune.Text = "Single tune";
            this.tsmiSingleTune.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // scMainSplit
            // 
            this.scMainSplit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scMainSplit.BackColor = System.Drawing.SystemColors.Control;
            this.scMainSplit.Location = new System.Drawing.Point(0, 28);
            this.scMainSplit.Name = "scMainSplit";
            this.scMainSplit.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scMainSplit.Panel1
            // 
            this.scMainSplit.Panel1.Controls.Add(this.scMessagesSplit);
            // 
            // scMainSplit.Panel2
            // 
            this.scMainSplit.Panel2.Controls.Add(this.groupBox1);
            this.scMainSplit.Size = new System.Drawing.Size(821, 359);
            this.scMainSplit.SplitterDistance = 178;
            this.scMainSplit.SplitterWidth = 3;
            this.scMainSplit.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btConsoleSend);
            this.groupBox1.Controls.Add(this.cbConsoleWriter);
            this.groupBox1.Controls.Add(this.tbConsoleViewer);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(821, 178);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Console";
            // 
            // btConsoleSend
            // 
            this.btConsoleSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btConsoleSend.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveBorder;
            this.btConsoleSend.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btConsoleSend.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlLight;
            this.btConsoleSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btConsoleSend.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btConsoleSend.Location = new System.Drawing.Point(765, 151);
            this.btConsoleSend.Margin = new System.Windows.Forms.Padding(1);
            this.btConsoleSend.Name = "btConsoleSend";
            this.btConsoleSend.Size = new System.Drawing.Size(52, 23);
            this.btConsoleSend.TabIndex = 1;
            this.btConsoleSend.Text = "Send";
            this.btConsoleSend.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btConsoleSend.UseVisualStyleBackColor = false;
            this.btConsoleSend.Click += new System.EventHandler(this.btConsoleSend_Click);
            // 
            // cbConsoleWriter
            // 
            this.cbConsoleWriter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbConsoleWriter.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbConsoleWriter.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConsoleWriter.FormattingEnabled = true;
            this.cbConsoleWriter.Location = new System.Drawing.Point(3, 151);
            this.cbConsoleWriter.Name = "cbConsoleWriter";
            this.cbConsoleWriter.Size = new System.Drawing.Size(758, 23);
            this.cbConsoleWriter.TabIndex = 3;
            this.cbConsoleWriter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbConsoleWriter_KeyPress);
            // 
            // tbConsoleViewer
            // 
            this.tbConsoleViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbConsoleViewer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbConsoleViewer.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbConsoleViewer.Location = new System.Drawing.Point(3, 19);
            this.tbConsoleViewer.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.tbConsoleViewer.Multiline = true;
            this.tbConsoleViewer.Name = "tbConsoleViewer";
            this.tbConsoleViewer.ReadOnly = true;
            this.tbConsoleViewer.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbConsoleViewer.Size = new System.Drawing.Size(812, 132);
            this.tbConsoleViewer.TabIndex = 2;
            this.tbConsoleViewer.WordWrap = false;
            // 
            // timerStatus
            // 
            this.timerStatus.Enabled = true;
            this.timerStatus.Interval = 5000;
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // MainGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 412);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.scMainSplit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(500, 300);
            this.Name = "MainGUI";
            this.Text = "CMSniffer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainGUI_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.scMessagesSplit.Panel1.ResumeLayout(false);
            this.scMessagesSplit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scMessagesSplit)).EndInit();
            this.scMessagesSplit.ResumeLayout(false);
            this.cmsParList.ResumeLayout(false);
            this.scMainSplit.Panel1.ResumeLayout(false);
            this.scMainSplit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scMainSplit)).EndInit();
            this.scMainSplit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripButton btConsole;
        private System.Windows.Forms.SplitContainer scMessagesSplit;
        private System.Windows.Forms.ListView lvMessagesList;
        private System.Windows.Forms.ListView lvMessageInfo;
        private System.Windows.Forms.SplitContainer scMainSplit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btConsoleSend;
        private System.Windows.Forms.TextBox tbConsoleViewer;
        private System.Windows.Forms.ComboBox cbConsoleWriter;
        private System.Windows.Forms.ToolStripButton btListChannels;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lbSerialStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel lbCMSStatus;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel lbStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ContextMenuStrip cmsParList;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddTune;
        private System.Windows.Forms.ToolStripMenuItem tsmiSingleTune;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSplitButton btConnect;
        private System.Windows.Forms.ToolStripMenuItem sourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox tbSourceID;
        private System.Windows.Forms.ToolStripMenuItem destinationIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox tbDestinationID;
        private System.Windows.Forms.ToolStripSplitButton btSingleTune;
        private System.Windows.Forms.ToolStripMenuItem tuneIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recordIDToolStripMenuItem1;
        private System.Windows.Forms.ToolStripComboBox cbSerialPort;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripComboBox cbSelectedTunes;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripTextBox tbTuneID;
        private System.Windows.Forms.ToolStripTextBox tbRecordID;
        private System.Windows.Forms.ToolStripMenuItem tsmiClearTunes;
        private System.Windows.Forms.Timer timerStatus;

    }
}

