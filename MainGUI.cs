﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.Collections.Concurrent;
using System.Timers;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace CMSniffer
{
    public partial class MainGUI : Form
    {
        private int consoleHeight;
        ThreadCOM serialthread;
        private ArrayList selectedMessagesID;

        private MainClass mainClass;

        /* Status messages queue */
        private ConcurrentQueue<String> statusQueue = new ConcurrentQueue<String>();

        /* New received messages */
        public ConcurrentQueue<Message> newReceivedMessages = new ConcurrentQueue<Message>();

        /* Interclass calls type ID */
        public enum GUIStatus { PortOpened, CMSConnected, CMSDisconnected, AllClosed, Wainting };

        /* Constructor */
        public MainGUI()
        {
            InitializeComponent();

            mainClass = new MainClass(this);
            serialthread = mainClass.threadCOM;
            selectedMessagesID = new ArrayList();

            /* Init interface */
            SetGUIStatus(GUIStatus.AllClosed);
            consoleHeight = scMainSplit.Panel2.Height + scMainSplit.SplitterWidth;
            scMainSplit.Panel2Collapsed = true;
        }

        private void timerStatus_Tick(object sender, EventArgs e)
        {
            string output = "";
            while (true)
            {
                if (statusQueue.TryDequeue(out output))
                {
                    if (output != lbStatus.Text)
                        break;
                }
                else
                {
                    output = "";
                    break;
                }
            }

            lbStatus.Text = output;
        }

        private void enqueueStatus(string msg)
        {
            statusQueue.Enqueue(msg);

            if (lbStatus.Text.Trim() == "")
            {
                timerStatus.Stop();
                timerStatus.Start();
                timerStatus_Tick(null, null);
            }
        }

        private void SetGUIStatus(GUIStatus status)
        {
            switch (status)
            {
                case GUIStatus.PortOpened:
                    /* Connection Button string */
                    btConnect.Text = "Disconnect";

                    /* Status string */
                    lbSerialStatus.Text = "openned";

                    /* Interface lock */
                    btConnect.Enabled =
                        true;
                    scMainSplit.Enabled =
                        cbSerialPort.Enabled =
                        tbSourceID.Enabled =
                        tbDestinationID.Enabled =
                        btListChannels.Enabled =
                        cbSelectedTunes.Enabled =
                        btSingleTune.Enabled = 
                        false;
                    break;
                case GUIStatus.CMSConnected:
                    /* Connection Button string */
                    btConnect.Text = "Disconnect";

                    /* Status string */
                    lbCMSStatus.Text = "connected";
                    lbSerialStatus.Text = "openned";
                    
                    /* Interface unlock */
                    scMainSplit.Enabled =
	                    btConnect.Enabled =
	                    btListChannels.Enabled =
	                    cbSelectedTunes.Enabled =
	                    btSingleTune.Enabled = 
	                    true;
                    cbSerialPort.Enabled =
                        tbSourceID.Enabled =
                        tbDestinationID.Enabled =
	                    false;
                    break;
                case GUIStatus.CMSDisconnected:
                    /* Connection Button string */
                    btConnect.Text = "Connect";

                    /* Status string */
                    lbCMSStatus.Text = "disconnect";

                    /* Status string */
                    lbSerialStatus.Text = "closed";

                    /* Interface lock */
                    btConnect.Enabled =
                        true;
                    scMainSplit.Enabled =
                        tbSourceID.Enabled =
                        tbDestinationID.Enabled =
	                    cbSerialPort.Enabled =
	                    btListChannels.Enabled =
	                    cbSelectedTunes.Enabled =
	                    btSingleTune.Enabled =
	                    false;
                    break;
                case GUIStatus.AllClosed:
                    /* Connection Button string */
                    btConnect.Text = "Connect";

                    /* Status string */
                    lbCMSStatus.Text = "disconnect";

                    /* Status string */
                    lbSerialStatus.Text = "closed";

                    /* Interface lock */
                    cbSerialPort.Enabled =
                        tbSourceID.Enabled =
                        tbDestinationID.Enabled =
                        btConnect.Enabled =
	                    true;
                    scMainSplit.Enabled =
	                    btListChannels.Enabled =
	                    cbSelectedTunes.Enabled =
	                    btSingleTune.Enabled = 
	                    false;
                    break;
                case GUIStatus.Wainting:
                    scMainSplit.Enabled =
                        tbSourceID.Enabled =
                        tbDestinationID.Enabled =
                        cbSerialPort.Enabled =
                        btConnect.Enabled =
                        btListChannels.Enabled =
                        cbSelectedTunes.Enabled =
                        btSingleTune.Enabled =
                        false;

                    break;
                default: break;
            }
        }

        private void sendMessage(string origmsg)
        {
            string msg = Regex.Replace(origmsg, "[^a-fA-F0-9]", string.Empty);

            if ((msg.Length % 2) == 1)
            {
                enqueueStatus("Error: command should not have odd number of hexadecimal chars. Aborting");
                return;
            }
            if ((msg.Length % 4) != 0)
            {
                enqueueStatus("Error: command should not have odd number of bytes. Aborting");
                return;
            }
            if (origmsg.Trim().ToUpper() != msg.Trim().ToUpper())
            {
                enqueueStatus("Warning: command seems to have invalid chars which were removed. Aborting");
                cbConsoleWriter.Text = msg;
                return;
            }

            byte[] cmd = new byte[msg.Length / 2];

            for (int index = 0; index < cmd.Length; index++)
                cmd[index] = byte.Parse(msg.Substring(index * 2, 2), NumberStyles.HexNumber);

            cbConsoleWriter.Items.Add(cbConsoleWriter.Text);
            cbConsoleWriter.Text = "";

            mainClass.threadCOM.sendRequest(cmd);
        }

        /* MainGUI Form's Events Handlers */
        /* Closing Event */
        private void MainGUI_FormClosing(object sender, EventArgs e)
        {
            mainClass.shutdownAll();
        }

        /* Lists available Serial Ports */
        private void cbSerialPort_Click(object sender, EventArgs e)
        {
            cbSerialPort.Items.Clear();
            foreach (string s in System.IO.Ports.SerialPort.GetPortNames())
                cbSerialPort.Items.Add(s);
        }

        /* Starts a new connection */
        private void btConnect_Click(object sender, EventArgs e)
        {
            if (tbTuneID.Text.Trim().Length <= 0)
                tbTuneID.Text = tbSourceID.Text.Trim();

            /* If there is no selected serial port, select the first found */
            if (cbSerialPort.Text.Trim().Length <= 0 && System.IO.Ports.SerialPort.GetPortNames().Length > 0)
            {
                cbSerialPort.Text = System.IO.Ports.SerialPort.GetPortNames()[0];
                enqueueStatus("Warning: no serial port selected; using first found");
            }
            
            mainClass.changeConnectionState(cbSerialPort.Text.Trim(), tbSourceID.Text.Trim(), tbDestinationID.Text.Trim());
        }

        /* Sends a Par_List request */
        private void btListChannels_Click(object sender, EventArgs e)
        {
            mainClass.sendParListRequest();
        }

        /* Sends a Single_Tune request */
        private void btSingleTune_Click(object sender, EventArgs e)
        {
            /* NEW */
            mainClass.sendSingleTune((Msg_Par_List.MsgId[])selectedMessagesID.ToArray(typeof(Msg_Par_List.MsgId)),
                byte.Parse(tbRecordID.Text.Trim()),
                ushort.Parse(tbTuneID.Text.Trim()));

            return;
            /* OLD */
            Msg_Par_List.MsgId messageId = new Msg_Par_List.MsgId(0x800A, 0x01, 0x8006, 0x01, 0x0003, 0x01, "");
            Msg_Par_List.MsgId[] messagesId = new Msg_Par_List.MsgId[3];
            messagesId[0] = messageId;
            messagesId[1] = new Msg_Par_List.MsgId(0x800A, 0x01, 0x8006, 0x02, 0x0003, 0x01, "");
            messagesId[2] = new Msg_Par_List.MsgId(0x800A, 0x01, 0x8006, 0x03, 0x0003, 0x01, "");
            byte recId = 0x00;
            //mainClass.sendSingleTune(messagesId, recId);

            //TODO mandar as Msg_Par_List.MsgId[] a enviar e ir buscar o valor do recId
        }

        /* Shows/hides console viewer */
        private void btConsole_Click(object sender, EventArgs e)
        {
            if (scMainSplit.Panel2Collapsed)
            {
                this.Height += consoleHeight;
                scMainSplit.Panel2Collapsed = false;
                //scMainSplit.Panel2.Show();
            }
            else
            {
                consoleHeight = scMainSplit.Panel2.Height;
                this.Height -= consoleHeight;
                scMainSplit.Panel2Collapsed = true;
                //scMainSplit.Panel2.Hide();

            }
        }

        /* Sends when enter key is stroked */
        private void cbConsoleWriter_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                sendMessage(cbConsoleWriter.Text);
            }
        }

        /* Console send button pressed */
        private void btConsoleSend_Click(object sender, EventArgs e)
        {
            sendMessage(cbConsoleWriter.Text);
        }

        /* Message Select Changed */
        private void lvMessagesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvMessagesList.SelectedIndices.Count != 1)
                return;
            int selectedIndex = lvMessagesList.SelectedIndices[0];
            Message selectedMessage = (Message)mainClass.receivedMessages[selectedIndex];
            selectedMessage.setToListView(lvMessageInfo);

            /* Enables or disables context menu */
            if (selectedMessage.GetType() == typeof(Msg_Par_List) && selectedMessage.Type == Message.MessageType.Response)
            {
                lvMessageInfo.ContextMenuStrip = cmsParList;
                lvMessageInfo.MultiSelect = true;
            }
            else
            {
                lvMessageInfo.ContextMenuStrip = null;
                lvMessageInfo.MultiSelect = false;
            }
        }

        /* Update Selected Tunes ComboBox list */
        private void cbSelectedTunes_Click(object sender, EventArgs e)
        {
            cbSelectedTunes.Items.Clear();

            foreach (Msg_Par_List.MsgId msgId in selectedMessagesID)
            {
                cbSelectedTunes.Items.Add(msgId.ascii);
            }
        }

        /* Add tune to selected tunes */
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            /* Check erros */
            if (lvMessagesList.SelectedItems.Count != 1)
                return;

            Msg_Par_List selectedParList = (Msg_Par_List)mainClass.receivedMessages[lvMessagesList.SelectedIndices[0]];

            foreach (int i in lvMessageInfo.SelectedIndices)
                selectedMessagesID.Add(selectedParList.listInfo[i]);
        }

        /* Single tune selected item(s) */
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            /* Check erros */
            if (lvMessagesList.SelectedItems.Count != 1)
                return;
 
            Msg_Par_List selectedParList = (Msg_Par_List)mainClass.receivedMessages[lvMessagesList.SelectedIndices[0]];
            Msg_Par_List.MsgId[] selectedMsgId = new Msg_Par_List.MsgId[lvMessageInfo.SelectedItems.Count];

            int position = 0;
            foreach (int i in lvMessageInfo.SelectedIndices)
                selectedMsgId[position++] = (Msg_Par_List.MsgId)selectedParList.listInfo[i];

            mainClass.sendSingleTune(selectedMsgId,
                byte.Parse(tbRecordID.Text.Trim()),
                ushort.Parse(tbTuneID.Text.Trim()));
        }

        /* Double click sends a single tune request */
        private void lvMessageInfo_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            /* Check erros */
            if (lvMessagesList.SelectedItems.Count != 1 ||
                    lvMessageInfo.SelectedItems.Count < 1 ||
                    lvMessageInfo.SelectedItems.Count > 10)
                return;

            Msg_Par_List selectedParList = (Msg_Par_List)mainClass.receivedMessages[lvMessagesList.SelectedIndices[0]];
            Msg_Par_List.MsgId[] selectedMsgId = new Msg_Par_List.MsgId[lvMessageInfo.SelectedItems.Count];

            int position = 0;
            foreach (int i in lvMessageInfo.SelectedIndices)
                selectedMsgId[position++] = (Msg_Par_List.MsgId)selectedParList.listInfo[i];

            mainClass.sendSingleTune(selectedMsgId,
                byte.Parse(tbRecordID.Text.Trim()),
                ushort.Parse(tbTuneID.Text.Trim()));
        }

        /* Add no more then 10 tunes */
        private void cmsParList_Opening(object sender, CancelEventArgs e)
        {
            if ((selectedMessagesID.Count + lvMessageInfo.SelectedItems.Count) <= 10 &&
                    (selectedMessagesID.Count + lvMessageInfo.SelectedItems.Count) > 0)
                tsmiAddTune.Enabled = true;
            else
                tsmiAddTune.Enabled = false;

            if (lvMessageInfo.SelectedItems.Count < 1 ||
                    lvMessageInfo.SelectedItems.Count > 10)
                tsmiSingleTune.Enabled = false;
            else
                tsmiSingleTune.Enabled = true;
        }

        /* Clear selected tunes */
        private void tsmiClearTunes_Click(object sender, EventArgs e)
        {
            selectedMessagesID.Clear();
            cbSelectedTunes.Text = "";
        }

        /* Delegates */
        /* Interclass GUI Update delegate */
        private delegate void GUIUpdateDelegate(MainClass.Call action, object obj);
        public void GUIUpdate(MainClass.Call action, object obj)
        {
            if (InvokeRequired)
                BeginInvoke(new GUIUpdateDelegate(InnerGUIUpdate), new object[] { action, obj });
            else
                InnerGUIUpdate(action, obj);
        }
        private void InnerGUIUpdate(MainClass.Call action, object obj)
        {
            switch (action)
            {
                case MainClass.Call.ConnectionSucceed: ConnectionSucceed(); break;
                case MainClass.Call.DisconnectSucceed: DisconnectSucceed((string)obj); break;
                case MainClass.Call.NewMessage: NewMessage(); break;
                case MainClass.Call.UpdateLastMessage: UpdateLastMessage(); break;
                case MainClass.Call.WriteConsole: ConsoleWrite((string)obj); break;
                case MainClass.Call.OpenPortError: OpenPortError((string)obj); break;
                case MainClass.Call.OpenedPort: OpenedPort(); break;
                case MainClass.Call.ClosedPort: ClosedPort(); break;
                default: break;
            }
        }

        /* Innerclass calls handlers */
        /* Connection Succeed */
        private void ConnectionSucceed()
        {
            SetGUIStatus(GUIStatus.CMSConnected);
        }

        /* Disconnection Succeed */
        private void DisconnectSucceed(string msg)
        {
            SetGUIStatus(GUIStatus.AllClosed);
        }

        /* New message received */
        private void NewMessage()
        {
            /* Add last received message to the messages' listview and write message to console */
            while (newReceivedMessages.Count > 0)
            {
                Message message;
                if (!newReceivedMessages.TryDequeue(out message))
                    return;

                lvMessagesList.Items.Add(message.listViewTitle);
                tbConsoleViewer.AppendText(message.ToString() + "\r\n");
            }


            /* Select new received message */
            lvMessagesList.Items[lvMessagesList.Items.Count - 1].Selected = true;
            lvMessagesList.Items[lvMessagesList.Items.Count - 1].EnsureVisible();
        }

        /* Update last message received */
        private void UpdateLastMessage()
        {
            /* Print that message's info */
            lvMessagesList.Items[lvMessagesList.Items.Count - 1].Selected = true;
            lvMessagesList_SelectedIndexChanged(null, null);
        }

        /* Write message to console */
        private void ConsoleWrite(string msg)
        {
            tbConsoleViewer.AppendText(msg + "\r\n");
        }

        /* Open SerialPort error */
        private void OpenPortError(string msg)
        {
            enqueueStatus(msg);
            SetGUIStatus(GUIStatus.AllClosed);
        }

        /* Opened port */
        private void OpenedPort()
        {
            SetGUIStatus(GUIStatus.PortOpened);
        }

        /* Closed port */
        private void ClosedPort()
        {
            SetGUIStatus(GUIStatus.AllClosed);
        }

    }
}

