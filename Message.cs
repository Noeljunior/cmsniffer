﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace CMSniffer
{
    public abstract partial class Message
    {
        /* Transfer Header */
        public Trans_Hd trans_Hd;

        /* Trans_Hd Structure */
        public struct Trans_Hd
        {
            public ushort lenght { get; set; }
            public ushort destinationID { get; set; }
            public ushort sourceID { get; set; }
            public ushort command { get; set; }

            public Trans_Hd(ushort lenght, ushort destinationID, ushort sourceID, ushort command)
                : this()
            {
                this.lenght = lenght;
                this.destinationID = destinationID;
                this.sourceID = sourceID;
                this.command = command;
            }
        }

        /* ListView Items */
        protected ArrayList listViewItems;
        public string listViewTitle { get; set; }

        /* Message Type */
        public enum MessageType { Request, Response, Notification, Data };
        public MessageType Type;

        /* Constructors */
        public Message(Trans_Hd trans_Hd, MessageType Type)
        {
            this.trans_Hd = trans_Hd;
            this.Type = Type;

            listViewItems = new ArrayList();
        }

        protected string HeaderToString(string command)
        {
            switch (Type) {
                case MessageType.Request: command += "_REQ"; break;
                case MessageType.Response: command += "_RSP"; break;
                case MessageType.Notification: command += "_NOTIFICATION"; break;
                case MessageType.Data: command += "_DATA"; break;
            }

            return String.Format("[{0,-21}] ({1,3}) <{2,5}> <{3,5}> [{4,5}] ",
                command,
                trans_Hd.lenght, trans_Hd.sourceID, trans_Hd.destinationID, trans_Hd.command);
        }

        /* Asbtracts - to be implements on derived classes */
        /* Fill the object by byte array stream */
        public abstract void fillByByteArray(byte[] raw);

        /* Returns the objects in a string */
        public override abstract string ToString();

        /* Fill a ListView */
        public abstract void setToListView(System.Windows.Forms.ListView listview);
    }
}
