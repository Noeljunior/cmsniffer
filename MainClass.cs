﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace CMSniffer
{
    public class MainClass
    {
        /* An array of received messages */
        public ArrayList receivedMessages { get; set; }

        /* An hash et of known Tunes */
        public HashSet<Msg_Tune.Tune> knownTunes;
        
        /* Reference to comunication thread */
        public ThreadCOM threadCOM { get; set; }

        /* Reference to MainGUI form */
        public MainGUI mainGUI { get; set; }

        /* Transfer source and destination IDs */
        public ushort sourceID { get; set; }
        public ushort destinationID { get; set; }

        /* Interclass calls type ID */
        public enum Call { WriteToStatusQueue, ConnectionSucceed, NewMessage, UpdateLastMessage, DisconnectSucceed, WriteConsole, OpenPortError, OpenedPort, ClosedPort };

        /* Constructor */
        public MainClass(MainGUI mainGUI)
        {
            this.mainGUI = mainGUI;

            /* Initializates objects */
            threadCOM = new ThreadCOM(this);
            receivedMessages = new ArrayList();
            knownTunes = new HashSet<Msg_Tune.Tune>();

            /* DEBUG */
            System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\debug.txt", true);
            file.WriteLine("\r\n\r\n\r\n");
            file.Close();
        }

        /* Check for MBPs in known tunes */
        public bool checkForKnownTune(ushort mbp)
        {
            foreach (Msg_Tune.Tune tune in knownTunes)
                if (tune.mpbHd == mbp)
                    return true;

            return false;
        }

        /* New conncetion */
        public void changeConnectionState(string COM, string sourceID, string destinationID)
        {
            /* Check if port is still openned */
            if (threadCOM.isOpen())
            {
                /* Try to close it */
                sendDisconnectRequest();
                //threadCOM.CloseSerialPort();
                
                /* Notify GUI that port is closed */
                //mainGUI.GUIUpdate(Call, null);
            }
            else /* Else, try to stablish a new connection */
            {

                if (!threadCOM.OpenSerialPort(COM)) /* Error openning SerialPort */
                {
                    return;
                }

                /* Notify GUI that port is opened */
                mainGUI.GUIUpdate(Call.OpenedPort, null);

                this.sourceID = ushort.Parse(sourceID.Trim());
                this.destinationID = ushort.Parse(destinationID.Trim()); ;

                /* Send Connection Request */
                sendConnectRequest();
            }
        }


        /* Requests' senders */
        /* Par List Request */
        public void sendParListRequest()
        {
            threadCOM.sendRequest(Msg_Par_List.Request());
        }

        /* Single Tune Request */
        public void sendSingleTune(Msg_Par_List.MsgId[] messagesId, byte recId, ushort tuneID)
        {
            threadCOM.sendRequest(Msg_Tune.Request(messagesId, recId, tuneID));
        }

        /* Connect Request */
        public void sendConnectRequest()
        {
            threadCOM.sendRequest(Msg_Connect.Request("0"));
        }

        /* Disconnect Request */
        public void sendDisconnectRequest()
        {
            threadCOM.sendRequest(Msg_Disconnect.Request());
        }

        /* Closes all connections, threads and objects */
        public void shutdownAll()
        {
            sendDisconnectRequest();
            threadCOM.Shutdown();
        }


        /* Add a message to the messages' ArrayList */
        public void addMessage(Message message)
        {
            /* Check something before adding */
            switch (message.trans_Hd.command)
            {
                case 12:     /* Par List Response */
                    /* Check if there is an uncompleted message to concatenate */

                    for (int i = receivedMessages.Count - 1; i >= 0; i--)
                    {
                        if (receivedMessages[i].GetType() == typeof(Msg_Par_List))
                        {
                            if (((Msg_Par_List)receivedMessages[i]).concatenate((Msg_Par_List)message))
                            {
                                mainGUI.GUIUpdate(Call.WriteConsole, message.ToString());
                                mainGUI.GUIUpdate(Call.UpdateLastMessage, null);
                                return;
                            }
                        }

                    }


                    break;

                case 16:    /* Single Tune Response */
                    /* Add known single tunes MBP's to known tunes */
                    foreach (Msg_Tune.Tune tune in ((Msg_Tune)message).listTunes)
                        knownTunes.Add(tune);
                    break;

                default:
                    break;
            }

            /* Add message to ArrayList and Console */
            receivedMessages.Add(message);
            mainGUI.newReceivedMessages.Enqueue(message);
            mainGUI.GUIUpdate(Call.NewMessage, null);
            //mainGUI.GUIUpdate(Call.WriteConsole, message.ToString());

            /* Update GUI */
            switch (message.trans_Hd.command)
            {
                case 2:     /* Connection Response */
                    /* Raise connection succeed update */
                    mainGUI.GUIUpdate(Call.ConnectionSucceed, null);
                    break;
                case 8:     /* Disconnection Response */
                    /* Raise disconnection update */
                    mainGUI.GUIUpdate(Call.DisconnectSucceed, null);
                    threadCOM.CloseSerialPort();
                    break;

                default:
                    break;
            }
        }
    }
}
