﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CMSniffer
{
    class Msg_Notifications : Message
    {

        /* Overflow parameters */
        public ushort error { get; set; }

        /* Notification Command ID */
        private static string OVERFLOW_NOTIFICATION = "5";
        private static string ABORT_NOTIFICATION = "6";

        /* Constructors */
        public Msg_Notifications(Trans_Hd trans_Hd, MessageType Type)
            : base(trans_Hd, Type)
        {
            /* Message Title */
            error = new ushort();
            listViewTitle = ((trans_Hd.command == 5 ? "Overflow " : "Abort ") + "Notification");
        }

        public override void fillByByteArray(byte[] raw)
        {
            /* Overflow Notification */
            if (trans_Hd.command == 5)
            {
                error = (ushort)(raw[0] << 8 | raw[1]);
                
                /* Manda imprimir dados Recebidos */
                //TODO 
                //addInfo(error);
                
                /* Pre-compute ListView Items */
                ListViewItem lvi = new ListViewItem(Message.DicGetVal(DictErrorToChar, error));
                listViewItems.Add(lvi);
            }

        }

        public override string ToString()
        {
            string output = "";

            if (trans_Hd.command == 5)
            {
                output = HeaderToString("OVERFLOW") + String.Format("[{0,4:X}]",error);
            }
            else
                output = HeaderToString("ABORT");
            
            return output;
        }

        /* Fill a ListView */
        public override void setToListView(System.Windows.Forms.ListView listview)
        {
            /* Clear previous ListView */
            listview.Clear();
            if (trans_Hd.command == 5) /* Overflow */
            {
                /* Set new columns' names */
                listview.Columns.Add("Error", 400, HorizontalAlignment.Center); //Error
            }

            /* Add new items to the ListView */
            foreach (ListViewItem lvi in listViewItems)
                listview.Items.Add(lvi);
        }

        /* Add a tune info to the arraylist of tunes */
        /*private void addInfo(ushort error)
        {
            /(error);
        }/**/

    }
}
