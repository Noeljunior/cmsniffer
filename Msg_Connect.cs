﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CMSniffer
{
    public class Msg_Connect : Message
    {
        /* Connect response parameters */
        public ushort windowsize { get; set; }
        public byte compathight { get; set; }
        public byte compatlow { get; set; }
        public byte ret { get; set; }
        public byte error { get; set; }

        /* Connect request parameters */
        public ushort tickperiod { get; set; } 
        

        /* Connect Request Command ID */
        private static string REQUEST_CMD = "1";
        private static string RESPONSE_CMD = "2";

        /* Constructors */
        public Msg_Connect(Trans_Hd trans_Hd, MessageType Type)
            : base(trans_Hd, Type)
        {
            /* Message Title */
            listViewTitle = "Connect " + (Type == MessageType.Request ? "Request" : "Response");
        }


        /* Fill the object by byte array stream */
        public override void fillByByteArray(byte[] raw)
        {
            if (Type == MessageType.Response) /* Response */
            {
                /* New connection info */
                windowsize = (ushort)(raw[0] << 8 | raw[1]);
                compathight = raw[2];
                compatlow = raw[3];
                ret = raw[5];
                error = raw[4];

                /* Pre-compute ListView Item */
                ListViewItem lvi = new ListViewItem(windowsize.ToString());
                lvi.SubItems.Add(compathight.ToString());
                lvi.SubItems.Add(compatlow.ToString());
                lvi.SubItems.Add(ret == 1 ? "succeed" : "failed");

                string errorstring = "";
                switch (error)
                {
                    case 0:
                        errorstring = "no error";
                        break;
                    case 1:
                        errorstring = "periodical life tick not implemented";
                        break;
                    case 2:
                        errorstring = "client ID out of range";
                        break;
                    default:
                        errorstring = "server specific error codes (ERNO: " + error + ")";
                        break;
                }
                lvi.SubItems.Add(errorstring);
                listViewItems.Add(lvi);
            }
            else /* Request */
            {
                tickperiod = (ushort)(raw[0] << 8 | raw[1]);

                /* Pre-compute ListView Item */
                ListViewItem lvi = new ListViewItem(tickperiod.ToString());
                listViewItems.Add(lvi);
            }
        }

        /* Fill a ListView */
        public override void setToListView(System.Windows.Forms.ListView listview)
        {
            /* Clear previous ListView */
            listview.Clear();

            /* Set new columns' names */
            if (Type == MessageType.Response)
            {
                listview.Columns.Add("Window Size", 75, HorizontalAlignment.Center);
                listview.Columns.Add("Compat Hight", 80, HorizontalAlignment.Center);
                listview.Columns.Add("Compat Low", 80, HorizontalAlignment.Center);
                listview.Columns.Add("Return", 80, HorizontalAlignment.Center);
                listview.Columns.Add("Error", 200, HorizontalAlignment.Center);
            }
            else
            {
                listview.Columns.Add("Tick Period", 80, HorizontalAlignment.Center);
            }

            /* Add new items to the ListView */
            foreach (ListViewItem lvi in listViewItems)
                listview.Items.Add(lvi);
        }

        /* Returns the objects in a string */
        public override string ToString()
        {
            string output = "";

            if (Type == MessageType.Request)
            {
                output = HeaderToString("CONNECT") +
                    String.Format(" {0}",
                    tickperiod);
            }
            else
            {
                output = HeaderToString("CONNECT") +
                    String.Format(" {0} {1} {2} {3} {4}",
                    windowsize, compathight, compatlow, ret, error);
            }

            return output;
        }

        /* Return the request's byte array */
        public static byte[] Request(string tickPeriod)
        {
            byte[] cmd = ThreadCOM.DecStringToByte(REQUEST_CMD);
            byte[] tickperiod = ThreadCOM.DecStringToByte(tickPeriod);

            byte[] connect_req = { cmd[1],          cmd[0],             // CMD
                                   tickperiod[1],   tickperiod[0] };    // Tick Period

            return connect_req;
        }
    }
}
