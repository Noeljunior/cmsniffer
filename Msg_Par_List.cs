﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CMSniffer
{


    public class Msg_Par_List : Message
    {
        /* ListInfo Array */
        public ArrayList listInfo { get; set; }

        /* Part number */
        public bool[] part;
        public byte actual;
        public byte total;

        /* Message ID Structure */
        public struct MsgId
        {
            public ushort sourceID { get; set; }
            public byte sourceNo { get; set; }
            public ushort channelID { get; set; }
            public byte channelNo { get; set; }
            public ushort msgType { get; set; }
            public byte layer { get; set; }
            public string ascii { get; set; }

            public MsgId(ushort sourceID, byte sourceNo, ushort channelID, byte channelNo, ushort msgType, byte layer, string ascii)
                : this()
            {
                this.sourceID = sourceID;
                this.sourceNo = sourceNo;
                this.channelID = channelID;
                this.channelNo = channelNo;
                this.msgType = msgType;
                this.layer = layer;
                this.ascii = ascii;
            }
        }

        /* Par List Request Command ID */
        private static string REQUEST_CMD = "11";
        private static string RESPONSE_CMD = "12";

        /* Constructors */
        public Msg_Par_List(Trans_Hd trans_Hd, MessageType Type)
            : base(trans_Hd, Type)
        {
            /* Objects init */
            listInfo = new ArrayList();
            listViewTitle = "Par List " + (Type == MessageType.Request ? "Request" : "Response");
        }

        /* Fill the object by byte array stream */
        public override void fillByByteArray(byte[] raw)
        {
            if (Type == MessageType.Response) /* Response */
            {
                string output;

                byte[] ascii = new byte[16];

                actual = raw[0];
                total = raw[1];

                part = new bool[total];
                part[actual-1] = true;

                for (int i = 2; i < raw.Length; i += 26)
                {
                    /* Ascii channel description */
                    Array.Copy(raw, 10 + i, ascii, 0, 16);
                    output = ThreadCOM.binToString(ascii);

                    addInfo((ushort)(raw[i] << 8 | raw[i + 1]),
                            raw[i + 6],
                            (ushort)(raw[i + 2] << 8 | raw[i + 3]),
                            raw[i + 7],
                            (ushort)(raw[i + 4] << 8 | raw[i + 5]),
                            raw[i + 8],
                            output.Trim());
                }

                /* Pre-compute ListView Items */
                foreach (MsgId li in listInfo)
                {
                    ListViewItem lvi = new ListViewItem(Message.DicGetVal(DictSourceIdToChar, li.sourceID));
                    lvi.SubItems.Add(li.sourceNo.ToString());
                    lvi.SubItems.Add(Message.DicGetVal(DictChannelIdToChar, li.channelID));
                    lvi.SubItems.Add(li.channelNo.ToString());
                    lvi.SubItems.Add(Message.DicGetVal(DictMsgTypeToChar, li.msgType));
                    lvi.SubItems.Add(li.layer.ToString());
                    lvi.SubItems.Add(li.ascii.ToString());
                    listViewItems.Add(lvi);
                }
            }
            else /* Request */
            {

            }
        }

        /* Concatenate a Msg_Par_List to this */
        public bool concatenate(Msg_Par_List msg)
        {
            if (Type == MessageType.Request || msg.Type == MessageType.Request) /* Cannot concatenate requests */
                return false;

            if (part.Length != msg.part.Length) /* Cannot concatenate two different size Par List */
                return false;

            for (int i = 0; i < part.Length; i++) /* Check if they don't represent the same part of message */
                if (part[i] == true && msg.part[i] == true)
                    return false;

            int numtrues = 0;
            for (int i = 0; i < part.Length; i++) /* Check if this is full */
            {
                if (part[i])
                    numtrues++;
            }

            if (numtrues == part.Length)
                return false;

            for (int i = 0; i < part.Length; i++) /* Concatenate part values */
                part[i] = part[i] || msg.part[i];

            /* Append data to the arraylists */
            listInfo.AddRange(msg.listInfo);
            listViewItems.AddRange(msg.listViewItems);

            return true;
        }

        /* Fill a ListView */
        public override void setToListView(System.Windows.Forms.ListView listview)
        {
            /* Clear previous ListView */
            listview.Clear();
            if (Type == MessageType.Response) /* Response */
            {
                /* Set new columns' names */
                listview.Columns.Add("Source ID", 120, HorizontalAlignment.Center);
                listview.Columns.Add("Source No", 75, HorizontalAlignment.Center);
                listview.Columns.Add("Channel ID", 120, HorizontalAlignment.Center);
                listview.Columns.Add("Channel No", 75, HorizontalAlignment.Center);
                listview.Columns.Add("Message Type", 85, HorizontalAlignment.Center);
                listview.Columns.Add("Layer", 50, HorizontalAlignment.Center);
                listview.Columns.Add("Description", 66);
            }
            else
            {

            }

            /* Add new items to the ListView */
            foreach (ListViewItem lvi in listViewItems)
                listview.Items.Add(lvi);
        }

        /* Returns the objects in a string */
        public override string ToString()
        {
            string output = "";

            if (Type == MessageType.Request)
            {
                output = HeaderToString("PAR_LIST");
            }
            else
            {
                String info = "";
                foreach (MsgId li in listInfo)
                {
                    info += String.Format("[{0,4:X} {1,4:X} {2,4:X} {3,2:X} {4,2:X} {5,2:X} {6,-8}] ",
                        li.sourceID,
                        li.channelID,
                        li.msgType,
                        li.channelNo,
                        li.sourceNo,
                        li.layer,
                        li.ascii.Trim());
                }

                output = HeaderToString("PAR_LIST") + String.Format("{0} {1} {2,0}",
                    actual, total, info.Trim());
            }

            return output;
        }

        /* Return the request's byte array */
        public static byte[] Request()
        {
            byte[] cmd = ThreadCOM.DecStringToByte(REQUEST_CMD);

            byte[] request = { cmd[1],          cmd[0], };    // Command

            return request;
        }

        /* Add a channel info to the arraylist of channels */
        private void addInfo(ushort sourceID, byte sourceNo, ushort channelID, byte channelNo, ushort msgType, byte layer, string ascii)
        {
            listInfo.Add(new MsgId(sourceID, sourceNo, channelID, channelNo, msgType, layer, ascii));
        }

    }

}