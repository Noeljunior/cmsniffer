﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMSniffer
{
    public abstract partial class Message
    {

        public static string DicGetVal(Dictionary<uint, string> d, ushort u)
        {
            string result = "";
            if (d.TryGetValue((uint)u, out result))
            {
                return result;
            }
            else
                return u.ToString();
           
        }

        public static Dictionary<uint, string> DictErrorToChar = new Dictionary<uint, string>()
        {
            { 1 , "window size overflow (RX RS232 -> TX MPB)"},
            { 2 , "TX buffer overflow (RX MPB -> TX RS232)"},
            { 3 , "RS232 HW-handshake malfunction"},
            { 4 , "MPB RX FIFO overflow (not used)"},
            { 5 , "RS232 RX parity error"},
            { 6 , "reserved for Computer rx_error: ABORT connection"},
            { 7 , "reserved for Computer rx_error: lifetick timeout "},
            { 8 , "reserved for Computer rx_error: lifetick failed"},
            { 9 , "reserved for Computer rx_error: no rsp on MIRROR_REQ"},
            { 10 , "reserved for Computer rx_error: no rsp on DIRECTORY_REQ"},
            { 12 , "reserved for Computer rx_error: no rsp on CONNECT_REQ"},
            { 13 , "reserved for Computer rx_error: no rsp on DISCONNECT_REQ"},
            { 14 , "reserved for Computer rx_error: message lost"},
            { 15 , "reserved for Computer rx_error: invalid command"},
            { 20 , "RS232 restart: MPB RX FIFO overflow"},
            { 21 , "RS232 restart: unspecific RS232 card fatal error"}

        };
        public static Dictionary<uint, string> DictSourceIdToChar = new Dictionary<uint, string>()
        {
            /***********************************************************************
            * SOURCE IDs [32768..49151]                                            *
            ***********************************************************************/

            { 32768U , "SRC_CardiacOutput" },
            { 32769U , "SRC_CO2" }, 
            { 32770U , "SRC_HeartModule" }, 
            { 32771U , "SRC_InvPress" }, 
            { 32772U , "SRC_NBP" }, 
            { 32773U , "SRC_O2" }, 
            { 32774U , "SRC_Pleth" }, 
            { 32775U , "SRC_Resp" }, 
            { 32776U , "SRC_SaO2" }, 
            { 32777U , "SRC_Temp" }, 
            { 32778U , "SRC_ThreeChanEcg" }, 
            { 32779U , "SRC_DeltaTemp" }, 
            { 32780U , "SRC_Wedge" }, 
            { 32781U , "SRC_EcgPacer" }, 
            { 32782U , "SRC_ThreeChanSt" }, 
            { 32783U , "SRC_TwoChanEeg" }, 
            { 32784U , "SRC_BpTool" }, 
            { 32785U , "SRC_Cpp" }, 
            { 32786U , "SRC_TcpO2" }, 
            { 32787U , "SRC_TcpCO2" }, 
            { 32788U , "SRC_SvO2" }, 
            { 32789U , "SRC_BloodAnalyzer" }, 
            { 32790U , "SRC_FourChanEeg" }, 
            { 32791U , "SRC_BloodReview" }, 
            { 32792U , "SRC_CCO" }, 
            { 32793U , "SRC_Bis" }, 

            { 32800U , "SRC_Sdn" }, 
            { 32801U , "SRC_Mhi" }, 
            { 32802U , "SRC_FeMgr" }, 
            { 32803U , "SRC_AlMgr" }, 
            { 32804U , "SRC_Arrhy" }, 
            { 32805U , "SRC_VCP" }, 
            { 32806U , "SRC_RecMgr" }, 
            { 32807U , "SRC_DateTime" }, 
            { 32808U , "SRC_Aoc" }, 
            { 32809U , "SRC_RestDsp" }, 
            { 32810U , "SRC_ErrorHdl" }, 
            { 32811U , "SRC_StatPromptHdl" }, 
            { 32812U , "SRC_Loa" }, 
            { 32813U , "SRC_RestingTrends" }, 
            { 32814U , "SRC_ExtendCRG" }, 
            { 32815U , "SRC_RvtApp" }, 
            { 32816U , "SRC_GlbInfoHdl" }, 
            { 32817U , "SRC_OpModHdl" }, 
            { 32818U , "SRC_CalTest" }, 
            { 32819U , "SRC_NumPos" }, 
            { 32820U , "SRC_Excalibur" }, 
            { 32821U , "SRC_ExcalUpcCalc" }, 
            { 32822U , "SRC_ExcalUpcTab" }, 
            { 32823U , "SRC_ExcalUpcTrend" }, 
            { 32824U , "SRC_ExcalUpcEad1" }, 
            { 32825U , "SRC_ExcalUpcEad2" }, 
            { 32826U , "SRC_ExcalUpcEph" }, 
            { 32827U , "SRC_ExcalEca" }, 
            { 32828U , "SRC_ExcalEdb" }, 
            { 32829U , "SRC_ExcalEda" }, 
            { 32830U , "SRC_ExcalEhc" }, 
            { 32832U , "SRC_ExcalUpcConfg" }, 
            { 32833U , "SRC_ExcalEdq" }, 
            { 32834U , "SRC_ExcalTransf" }, 
            { 32835U , "SRC_ExcalDrug" }, 
            { 32836U , "SRC_Aib" }, 
            { 32837U , "SRC_TabTrend" }, 
            { 32838U , "SRC_Printer" }, 
            { 32839U , "SRC_Snapshot" }, 

            { 32840U , "SRC_DelRecMgr" }, 
            { 32841U , "SRC_RtRecMgr" }, 
            { 32842U , "SRC_RecWaves" }, 
            { 32843U , "SRC_StoRecMgr" }, 

            { 32850U , "SRC_StripRec" }, 
            { 32851U , "SRC_SdnRec" }, 
            { 32852U , "SRC_FelpRec" }, 

            { 32854U , "SRC_GasAnalyzerCO2" }, 
            { 32855U , "SRC_GasAnalyzerO2" }, 
            { 32856U , "SRC_GasAnalyzerN2O" }, 
            { 32857U , "SRC_GasAnalyzerAgt" }, 

            { 32859U , "SRC_VentIf" }, 
            { 32860U , "SRC_AnalogOut" }, 
            { 32861U , "SRC_MipTypeB" }, 
            { 32862U , "SRC_MipTypeC" }, 
            { 32863U , "SRC_MipTypeA" }, 
            { 32864U , "SRC_Rs232Mgr" }, 
            { 32865U , "SRC_ParServer" }, 
            { 32866U , "SRC_TwServer" }, 
            { 32867U , "SRC_DAPServer" }, 
            { 32868U , "SRC_ABReview" }, 
            { 32869U , "SRC_AnMachIf" }, 
            { 32870U , "SRC_LanMgr" }, 
            { 32871U , "SRC_PVLoops" }, 
            { 32872U , "SRC_TouchBoard" }, 
            { 32873U , "SRC_TouchCalib" }, 


            /*----- additional Source Ids for TELEMETRY [35000..35499] -----------*/

            { 35000U , "SRC_MagicEcg" }, 
            { 35001U , "SRC_Inop" }, 
            { 35002U , "SRC_Service" }, 
            { 35003U , "SRC_Mua" }, 


            /*----- additional Source Ids for CENTRAL STATION [35500..35999] -----*/

            { 35500U , "SRC_SdnComm" }, 
            { 35501U , "SRC_SdnInt" }, 
            { 35502U , "SRC_SdnSys" }, 
            { 35503U , "SRC_UpcSys" }, 
            { 35504U , "SRC_PrDr" }, 
            { 35505U , "SRC_UpcAdmit" }, 
            { 35506U , "SRC_RdConfig" }, 
            { 35507U , "SRC_PrintMgr" }, 
            { 35508U , "SRC_Annot" }, 
            { 35509U , "SRC_UpcRd" }, 
            { 35510U , "SRC_DelayMgr" }, 
            { 35511U , "SRC_UpcRec" }, 
            { 35512U , "SRC_RecReq" }, 


            /*---------- additional Source Ids for NOMAD [36000..36099] ----------*/

            { 36000U , "SRC_NomadCtl" }, 


            /*--------- additional Source Ids for CATH LAB [36100..36199] --------*/


            { 49151U , "SRC_Unspec" }


        };


        public static Dictionary<uint, string> DictChannelIdToChar = new Dictionary<uint, string>()
        {
            /***********************************************************************
            * CHANNEL IDs [32768..49151]                                           *
            ***********************************************************************/

            { 32768U , "CHA_AWRR" }, 
            { 32769U , "CHA_BeatDetect" }, 
            { 32770U , "CHA_CO2" }, 
            { 32771U , "CHA_CuffPress" }, 
            { 32772U , "CHA_ETCO2" }, 
            { 32773U , "CHA_EarlySyst" }, 
            { 32774U , "CHA_Ecg" }, 
            { 32775U , "CHA_FIO2" }, 
            { 32776U , "CHA_General" }, 
            { 32777U , "CHA_HeartRate" }, 
            { 32778U , "CHA_IMCO2" }, 
            { 32779U , "CHA_OxygenSatur" }, 
            { 32780U , "CHA_Pleth" }, 
            { 32781U , "CHA_Press" }, 
            { 32782U , "CHA_PressNum" }, 
            { 32783U , "CHA_Pulse" }, 
            { 32784U , "CHA_PulseRate" }, 
            { 32785U , "CHA_RelativePerf" }, 
            { 32786U , "CHA_RespDetect" }, 
            { 32787U , "CHA_Temp" }, 
            { 32788U , "CHA_CardiacOutput" }, 
            { 32789U , "CHA_Resp" }, 
            { 32790U , "CHA_St" }, 
            { 32791U , "CHA_Eeg" }, 
            { 32792U , "CHA_TcpO2" }, 
            { 32793U , "CHA_TcpCO2" }, 
            { 32794U , "CHA_SiteTime" }, 
            { 32795U , "CHA_SvO2" }, 
            { 32796U , "CHA_Quality" }, 
            { 32797U , "CHA_DeltaOxygenSat" }, 
            { 32798U , "CHA_BloodNum" }, 

            { 32800U , "CHA_Arrhy" }, 
            { 32801U , "CHA_Overview" }, 
            { 32802U , "CHA_AutoAlDsp" }, 
            { 32803U , "CHA_EctSta" }, 
            { 32804U , "CHA_RhySta" }, 
            { 32805U , "CHA_Vpb" }, 
            { 32806U , "CHA_SdnText" }, 
            { 32807U , "CHA_SdnTime" }, 

            { 32809U , "CHA_Text" }, 
            { 32810U , "CHA_Config" }, 
            { 32811U , "CHA_Device" }, 
            { 32812U , "CHA_Fast" }, 
            { 32813U , "CHA_Slow" }, 
            { 32814U , "CHA_Control" }, 
            { 32815U , "CHA_Scan" }, 
            { 32816U , "CHA_Sort" }, 
            { 32817U , "CHA_Async" }, 
            { 32818U , "CHA_Sync" }, 
            { 32819U , "CHA_Test" }, 
            { 32820U , "CHA_AlRes" }, 
            { 32821U , "CHA_AlStat" }, 
            { 32822U , "CHA_AlText" }, 
            { 32823U , "CHA_InText" }, 
            { 32824U , "CHA_NumEnh" }, 
            { 32825U , "CHA_NurseRelay" }, 
            { 32826U , "CHA_RemSil" }, 
            { 32827U , "CHA_ToHil" }, 
            { 32828U , "CHA_ToFields" }, 
            { 32829U , "CHA_MhiKeys" }, 
            { 32830U , "CHA_MipWave" }, 
            { 32831U , "CHA_MipNum" }, 
            { 32832U , "CHA_RemSus" }, 
            { 32833U , "CHA_VentNum" }, 
            { 32834U , "CHA_Data" }, 
            { 32835U , "CHA_VentWave" }, 
            { 32836U , "CHA_AnMachNum" }, 
            { 32837U , "CHA_AnMachWave" }, 

            { 32839U , "CHA_DAP" }, 
            { 32840U , "CHA_VCP" }, 
            { 32841U , "CHA_LVT" }, 
            { 32842U , "CHA_RVT" }, 
            { 32843U , "CHA_DBQ" }, 

            { 32844U , "CHA_Etm" }, 
            { 32845U , "CHA_Etb" }, 
            { 32846U , "CHA_UpcCalc" }, 
            { 32847U , "CHA_Ead1" }, 
            { 32849U , "CHA_Eph" }, 
            { 32850U , "CHA_Edb" }, 
            { 32851U , "CHA_Eda" }, 
            { 32852U , "CHA_Ehc" }, 
            { 32853U , "CHA_Eup" }, 
            { 32854U , "CHA_Eca" }, 
            { 32855U , "CHA_Edq" }, 
            { 32856U , "CHA_Edt" }, 

            { 32860U , "CHA_Recorder" }, 
            { 32861U , "CHA_Delayed" }, 
            { 32862U , "CHA_Realtime" }, 
            { 32863U , "CHA_Recording" }, 

            { 32870U , "CHA_DateTime" }, 
            { 32871U , "CHA_Settings" }, 
            { 32872U , "CHA_Internal" }, 
            { 32873U , "CHA_MhiSnap" }, 


            /*-------- additional Channel Ids for TELEMETRY [3xxxx..33399] -------*/

            { 32880U , "CHA_Stored" }, 
            { 32890U , "CHA_InopCtl" }, 
            { 32891U , "CHA_InopRd" }, 


            /*----- additional Channel Ids for CENTRAL STATION [33400..33999] ----*/

            { 33440U , "CHA_Gbn1" }, 
            { 33441U , "CHA_Gbn2" }, 
            { 33442U , "CHA_Gbn3" }, 
            { 33443U , "CHA_Gbn4" }, 
            { 33444U , "CHA_Gbn5" }, 
            { 33445U , "CHA_Gbn6" }, 
            { 33446U , "CHA_Gbn7" }, 
            { 33447U , "CHA_Gbn8" }, 
            { 33448U , "CHA_Gbn9" }, 
            { 33449U , "CHA_Gbn10" }, 
            { 33450U , "CHA_Gbn11" }, 
            { 33451U , "CHA_Gbn12" }, 
            { 33452U , "CHA_NoGbn" }, 
            { 33453U , "CHA_DelayAssign" }, 
            { 33454U , "CHA_DelayDeassign" }, 


            /*---------- additional Channel Ids for NOMAD [34000..34199] ---------*/


            /*-------- additional Channel Ids for CATH LAB [34200..34399] --------*/


            /*----------- more Channel Ids for MERLIN [40000..49151] -------------*/

            { 40000U , "CHA_N2O" }, 
            { 40001U , "CHA_INN2O" }, 
            { 40002U , "CHA_ETN2O" }, 
            { 40003U , "CHA_Agent" }, 
            { 40004U , "CHA_INAgent" }, 
            { 40005U , "CHA_ETAgent" }, 
            { 40006U , "CHA_O2" }, 
            { 40007U , "CHA_INO2" }, 
            { 40008U , "CHA_ETO2" }, 

            { 40020U , "CHA_Eeg1" }, 
            { 40021U , "CHA_Eeg2" }, 
            { 40022U , "CHA_Eeg3" }, 
            { 40023U , "CHA_Eeg4" }, 

            /* New  channels for the pCCO parameter.                              */

            { 40024U , "CHA_CCO" }, 
            { 40025U , "CHA_CCI" }, 
            { 40026U , "CHA_SVR" }, 
            { 40027U , "CHA_SV" }, 
            { 40028U , "CHA_SI" }, 
            { 40029U , "CHA_SVRI" }, 

            { 40031U , "CHA_ITBV" }, 
            { 40032U , "CHA_EVLW" }, 
            { 40033U , "CHA_CFI" }, 

            { 40034U , "CHA_Bis" }, 

            { 40030U , "CHA_RdaConfig" }, 

            { 40035U , "CHA_Touch" }, 
            { 40036U , "CHA_Mouse" }, 

            { 40037U , "CHA_Emg" }, 
            { 40038U , "CHA_Sqi" }, 

            { 49151U , "CHA_Unspec" }

        };

        public static Dictionary<uint, string> DictMsgTypeToChar = new Dictionary<uint, string>()
        {
            /***********************************************************************
            * TYPE IDs [1..65534]                                                  *
            ***********************************************************************/

            { 1U , "TYP_SpiPS" }, 
            { 2U , "TYP_SpiCW" },
            { 3U , "TYP_SpiWS" }, 
            { 4U , "TYP_SpiEV" }, 
            { 5U , "TYP_SpiBV" }, 
            { 6U , "TYP_SpiBS" }, 
            { 7U , "TYP_SpiNU" }, 
            { 8U , "TYP_SpiST" }, 
            { 9U , "TYP_SpiPR" }, 
            { 10U , "TYP_SpiAL" }, 
            { 11U , "TYP_SpiIN" }, 
            { 13U , "TYP_SpiOR" }, 
            { 14U , "TYP_SpiOP" }, 
            { 15U , "TYP_SpiTR" }, 
            { 16U , "TYP_SpiAN" }, 
            { 17U , "TYP_SpiMX" }, 
            { 18U , "TYP_SpiIC" }, 

            { 20U , "TYP_SdnBedlb" }, 
            { 21U , "TYP_SdnPni" }, 
            { 22U , "TYP_SdnTime" }, 
            { 23U , "TYP_SdnCW" }, 
            { 24U , "TYP_SdnWS" }, 

            { 25U , "TYP_Data" }, 
            { 26U , "TYP_Req" }, 
            { 27U , "TYP_Rsp" }, 

            { 30U , "TYP_Key" }, 

            { 31U , "TYP_EcgEasi" }, 

            { 40U , "TYP_AppOpnReq" }, 
            { 41U , "TYP_AppClsReq" }, 
            { 42U , "TYP_AppOpnChk" }, 
            { 43U , "TYP_AppClsChk" }, 
            { 44U , "TYP_Wv1Ctl" }, 
            { 45U , "TYP_Wv2Ctl" }, 
            { 46U , "TYP_LVTCtl" }, 
            { 47U , "TYP_VTWvAnnot" }, 

            { 50U , "TYP_Edb" }, 
            { 51U , "TYP_Eda" }, 
            { 52U , "TYP_Ehc" }, 
            { 53U , "TYP_Eup" }, 
            { 54U , "TYP_Eca" }, 
            { 55U , "TYP_Edt" }, 
            { 56U , "TYP_Eph" }, 
            { 58U , "TYP_Dyn" }, 
            { 59U , "TYP_Bdcst" }, 
            { 60U , "TYP_Etm" }, 
            { 61U , "TYP_Etb" }, 
            { 62U , "TYP_UpcCalc" }, 
            { 63U , "TYP_Ead1" }, 
            { 65U , "TYP_Edq" }, 

            { 68U , "TYP_Change" }, 
            { 69U , "TYP_Error" }, 
            { 70U , "TYP_Status" }, 
            { 71U , "TYP_Control" }, 
            { 72U , "TYP_Info" }, 
            { 73U , "TYP_Input" }, 
            { 74U , "TYP_Output" }, 

            { 75U , "TYP_Capability" }, 
            { 76U , "TYP_RecOpReq" }, 
            { 77U , "TYP_RecOpChk" }, 
            { 78U , "TYP_Config" }, 

            { 80U , "TYP_MhiOSAl" }, 
            { 81U , "TYP_MhiOSCapt" }, 
            { 82U , "TYP_MhiOSInstr" }, 
            { 83U , "TYP_MhiOSMark" }, 
            { 84U , "TYP_MhiOSMon" }, 
            { 85U , "TYP_MhiOSOvw" }, 
            { 86U , "TYP_MhiOSPar" }, 
            { 87U , "TYP_MhiOSPat" }, 
            { 88U , "TYP_MhiOSRec" }, 
            { 90U , "TYP_Numerics" }, 
            { 91U , "TYP_Speeds" }, 
            { 92U , "TYP_TraceMode" }, 
            { 93U , "TYP_Waves" }, 
            { 94U , "TYP_Colors" }, 

            { 100U , "TYP_MakInternal" }, 

            { 110U , "TYP_AibReq" }, 
            { 111U , "TYP_AibRsp" }, 

            { 120U , "TYP_RVTAppl" }, 
            { 121U , "TYP_RVTGen" }, 
            { 122U , "TYP_RVTDspCmplt" }, 
            { 123U , "TYP_RVTAppStChk" }, 
            { 124U , "TYP_RVTSesUpdChk" }, 
            { 125U , "TYP_RVTDspUpdChk" }, 
            { 126U , "TYP_RVTDspCtlChk" }, 
            { 127U , "TYP_RVTWvAnnReq" }, 

            { 128U , "TYP_Isu" }, 
            { 132U , "TYP_PicCtl" }, 
            { 134U , "TYP_CtlReq" }, 
            { 135U , "TYP_CtlChk" }, 
            { 136U , "TYP_MUAChk" }, 
            { 137U , "TYP_LdSetCtl" }, 
            { 138U , "TYP_ExtMonCtl" }, 
            { 141U , "TYP_Vcp" }, 
            { 142U , "TYP_Vtp" }, 
            { 144U , "TYP_BedCtl" }, 
            { 150U , "TYP_DlyReq" }, 
            { 151U , "TYP_DlyChk" }, 
            { 152U , "TYP_StoredReq" }, 
            { 153U , "TYP_StoredChk" }, 
            { 154U , "TYP_AnnotReq" }, 
            { 156U , "TYP_StopReq" }, 
            { 157U , "TYP_StopChk" }, 
            { 158U , "TYP_StopNotif" }, 
            { 160U , "TYP_ClrReq" }, 
            { 161U , "TYP_ClrChk" }, 
            { 162U , "TYP_FilterReq" }, 
            { 163U , "TYP_FilterChk" }, 
            { 164U , "TYP_DataReq" }, 
            { 165U , "TYP_DataChk" }, 
            { 166U , "TYP_TotalsReq" }, 
            { 167U , "TYP_TotalsChk" }, 

            { 168U , "TYP_VolCtl" }, 


            /*------- additional Type Ids for CENTRAL STATION [1000..1999] -------*/

            { 1000U , "TYP_DemoReq" }, 
            { 1001U , "TYP_Demographics" }, 
            { 1002U , "TYP_SdnBranch" }, 
            { 1003U , "TYP_SdnDspAssign" }, 
            /*                                               assignment             */
            { 1004U , "TYP_SdnUnitStat" }, 
            { 1005U , "TYP_PerseusMap" }, 
            { 1006U , "TYP_GbnAssign" }, 
            { 1007U , "TYP_GbnUnassign" }, 
            { 1008U , "TYP_FiltTime" }, 
            { 1009U , "TYP_Csa" }, 
            /*                                               arrhythmia             */
            { 1010U , "TYP_nsus" }, 
            { 1014U , "TYP_SdnOnline" }, 
            { 1015U , "TYP_TuneWv" }, 
            { 1016U , "TYP_TuneWvAck" }, 
            { 1017U , "TYP_StWvStop" }, 
            { 1018U , "TYP_SdnBL" }, 
            { 1019U , "TYP_PkdSdnBL" }, 
            { 1020U , "TYP_PkdSdnPni" }, 
            { 1021U , "TYP_SdnAS" }, 
            { 1022U , "TYP_SdnAT" }, 
            { 1023U , "TYP_PkdSdnAT" }, 
            { 1024U , "TYP_SdnIT" }, 
            { 1025U , "TYP_PkdSdnIT" }, 
            { 1026U , "TYP_SdnAR" }, 
            { 1027U , "TYP_SdnWV" }, 
            { 1028U , "TYP_PkdSdnWV" }, 
            { 1029U , "TYP_SdnWSD" }, 
            { 1030U , "TYP_PkdSdnWSD" }, 
            /*                                               Data                   */
            { 1031U , "TYP_SdnPV" }, 
            { 1032U , "TYP_SdnPSD" }, 
            /*                                               data                   */
            { 1033U , "TYP_PkdSdnPSD" }, 
            { 1034U , "TYP_Isb" }, 
            /*                                                - bed                 */
            { 1035U , "TYP_Isr" }, 
            /*                                                - recorder            */
            { 1036U , "TYP_PkdIsr" }, 
            { 1037U , "TYP_PkdIsu" }, 
            { 1038U , "TYP_MaintStatus" }, 
            { 1039U , "TYP_PkdCsa" }, 
            /*                                               arrhythmia             */
            { 1040U , "TYP_SdnSwHdr" }, 
            { 1041U , "TYP_SdnBackoff" }, 
            { 1042U , "TYP_RecReq" }, 
            { 1043U , "TYP_RecOp" }, 
            { 1044U , "TYP_TxReq" }, 
            { 1045U , "TYP_Ws" }, 
            { 1046U , "TYP_DataCap" }, 
            { 1047U , "TYP_AnnotData" }, 
            { 1048U , "TYP_FormName" }, 
            { 1049U , "TYP_PerConf" }, 
            { 1050U , "TYP_SectorAssign" }, 
            { 1051U , "TYP_DisplayForm" }, 
            { 1052U , "TYP_HRLimits" }, 
            { 1053U , "TYP_Discharge" }, 


            /*---------- additional Type Ids for CATH LAB [2000..2999] -----------*/


            { 65388U , "TYP_Int" }, 
            { 65492U , "TYP_Rw" }, 
            { 65501U , "TYP_Priv" }, 
            { 65534U , "TYP_Unspec" }
        };
    }
}
