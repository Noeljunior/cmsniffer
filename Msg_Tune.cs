﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace CMSniffer
{
    public class Msg_Tune : Message
    {

        /* Tune request/response  parameters */
        public ushort tuneId { get; set; }
        public ArrayList listTunes { get; set; }
        public struct Tune
        {
            public ushort sourceID { get; set; }
            public byte sourceNo { get; set; }
            public ushort channelID { get; set; }
            public byte channelNo { get; set; }
            public ushort msgType { get; set; }
            public byte layer { get; set; }
            public ushort mpbHd { get; set; }
            public byte recId { get; set; }
            /*public byte appRecLen { get; set; }
            public ushort word { get; set; } /**/

            public Tune(Msg_Par_List.MsgId messageId, ushort mpbHd, byte recId)
                : this()
            {
                this.sourceID = messageId.sourceID;
                this.sourceNo = messageId.sourceNo;
                this.channelID = messageId.channelID;
                this.channelNo = messageId.channelNo;
                this.msgType = messageId.msgType;
                this.layer = messageId.layer;
                this.mpbHd = mpbHd;
                this.recId = recId;
            }
        }

        /* Single tune Request Command ID */
        private static string REQUESTSINGLE_CMD = "15";
        private static string RESPONSESINGLE_CMD = "16";

        /* Constructors */
        public Msg_Tune(Trans_Hd trans_Hd, MessageType Type)
            : base(trans_Hd, Type)
        {
            /* Message Title */
            listTunes = new ArrayList();
            listViewTitle = "Single Tune " + (Type == MessageType.Request ? "Request" : "Response");
        }

        public override void fillByByteArray(byte[] raw)
        {
            /* Response */
            if (Type == MessageType.Response)
            {
                tuneId = (ushort)(raw[0] << 8 | raw[1]);

                for (int i = 2; i < raw.Length; i += 14)
                {
                    Msg_Par_List.MsgId messageId = new Msg_Par_List.MsgId((ushort)(raw[i] << 8 | raw[i + 1]),
                            raw[i + 6],
                            (ushort)(raw[i + 2] << 8 | raw[i + 3]),
                            raw[i + 7],
                            (ushort)(raw[i + 4] << 8 | raw[i + 5]),
                            raw[i + 8], "");
                    /*i+9 é unused
                    raw[i+13], este byte é para portas LAN -> deve ser zero
                    i+14 e i+15 é para word -> para portas Lan */

                    /* Manda imprimir dados Recebidos */
                    addInfo(messageId, (ushort)(raw[i + 10] << 8 | raw[i + 11]), raw[i + 12]);/**/
                }

                /* Pre-compute ListView Items */
                foreach (Tune tu in listTunes)
                {
                    ListViewItem lvi = new ListViewItem(tuneId.ToString());
                    lvi.SubItems.Add(Message.DicGetVal(DictSourceIdToChar, tu.sourceID));
                    lvi.SubItems.Add(tu.sourceNo.ToString());
                    lvi.SubItems.Add(Message.DicGetVal(DictChannelIdToChar, tu.channelID));
                    lvi.SubItems.Add(tu.channelNo.ToString());
                    lvi.SubItems.Add(Message.DicGetVal(DictMsgTypeToChar, tu.msgType));
                    lvi.SubItems.Add(tu.layer.ToString());
                    lvi.SubItems.Add(tu.mpbHd.ToString());
                    lvi.SubItems.Add(tu.recId.ToString());
                    listViewItems.Add(lvi);
                }
            }
            else /* Request */
            {
                tuneId = (ushort)(raw[0] << 8 | raw[1]);

                for (int i = 2; i < raw.Length; i += 14)
                {
                    Msg_Par_List.MsgId messageId = new Msg_Par_List.MsgId((ushort)(raw[i] << 8 | raw[i + 1]),
                            raw[i + 6],
                            (ushort)(raw[i + 2] << 8 | raw[i + 3]),
                            raw[i + 7],
                            (ushort)(raw[i + 4] << 8 | raw[i + 5]),
                            raw[i + 8], "");
                    /*i+9 é unused
                    raw[i+13], este byte é para portas LAN -> deve ser zero
                    i+14 e i+15 é para word -> para portas Lan */

                    /* Manda imprimir dados Enviados */
                    addInfo(messageId, (ushort)(raw[i + 10] << 8 | raw[i + 11]), raw[i + 12]);
                }

                /* Pre-compute ListView Items */
                foreach (Tune tu in listTunes)
                {
                    ListViewItem lvi = new ListViewItem(tuneId.ToString());
                    lvi.SubItems.Add(Message.DicGetVal(DictSourceIdToChar, tu.sourceID));
                    lvi.SubItems.Add(tu.sourceNo.ToString());
                    lvi.SubItems.Add(Message.DicGetVal(DictChannelIdToChar, tu.channelID));
                    lvi.SubItems.Add(tu.channelNo.ToString());
                    lvi.SubItems.Add(Message.DicGetVal(DictMsgTypeToChar, tu.msgType));
                    lvi.SubItems.Add(tu.layer.ToString());
                    lvi.SubItems.Add(tu.mpbHd.ToString());
                    lvi.SubItems.Add(tu.recId.ToString());
                    listViewItems.Add(lvi);
                }
            }
        }

        public override string ToString()
        {
            string output = "";
            if (Type == MessageType.Request)
            {
                String info = "";
                foreach (Tune tu in listTunes)
                {
                    info += String.Format("[{0,4:X} {1,4:X} {2,4:X} {3,2:X} {4,2:X} {5,2:X} {6,4:X} {7,2:X}] ",
                        tu.sourceID,
                        tu.channelID,
                        tu.msgType,
                        tu.channelNo,
                        tu.sourceNo,
                        tu.layer,
                        tu.mpbHd,
                        tu.recId);
                }

                output = HeaderToString("SINGLE_TUNE") + String.Format("{0} {1,0}",
                    tuneId, info.Trim());
            }
            else
            {
                String info = "";
                foreach (Tune tu in listTunes)
                {
                    info += String.Format("[{0,4:X} {1,4:X} {2,4:X} {3,2:X} {4,2:X} {5,2:X} {6,4:X} {7,2:X}] ",
                        tu.sourceID,
                        tu.channelID,
                        tu.msgType,
                        tu.channelNo,
                        tu.sourceNo,
                        tu.layer,
                        tu.mpbHd,
                        tu.recId);
                }
                output = HeaderToString("SINGLE_TUNE") + String.Format("{0} {1,0}",
                    tuneId, info.Trim());
            }

            return output;
        }

        /* Fill a ListView */
        public override void setToListView(System.Windows.Forms.ListView listview)
        {
            /* Clear previous ListView */
            listview.Clear();
            if (Type == MessageType.Response) /* Response */
            {
                /* Set new columns' names */
                listview.Columns.Add("Tune Id", 60, HorizontalAlignment.Center); //TuneId
                listview.Columns.Add("Source ID", 120, HorizontalAlignment.Center);
                listview.Columns.Add("Source No", 75, HorizontalAlignment.Center);
                listview.Columns.Add("Channel ID", 120, HorizontalAlignment.Center);
                listview.Columns.Add("Channel No", 75, HorizontalAlignment.Center);
                listview.Columns.Add("Message Type", 85, HorizontalAlignment.Center);
                listview.Columns.Add("Layer", 50, HorizontalAlignment.Center);
                listview.Columns.Add("MPB Header", 80, HorizontalAlignment.Center);
                listview.Columns.Add("SPI Record Id", 85, HorizontalAlignment.Center);
            }
            else
            {
                /* Set new columns' names */
                listview.Columns.Add("Tune Id", 60, HorizontalAlignment.Center); //TuneId
                listview.Columns.Add("Source ID", 120, HorizontalAlignment.Center);
                listview.Columns.Add("Source No", 75, HorizontalAlignment.Center);
                listview.Columns.Add("Channel ID", 120, HorizontalAlignment.Center);
                listview.Columns.Add("Channel No", 75, HorizontalAlignment.Center);
                listview.Columns.Add("Message Type", 85, HorizontalAlignment.Center);
                listview.Columns.Add("Layer", 50, HorizontalAlignment.Center);
                listview.Columns.Add("MPB Header", 80, HorizontalAlignment.Center);
                listview.Columns.Add("SPI Record Id", 85, HorizontalAlignment.Center);
            }
            /* Add new items to the ListView */
            foreach (ListViewItem lvi in listViewItems)
                listview.Items.Add(lvi);
        }

        /* Return the request's byte array */
        public static byte[] Request(Msg_Par_List.MsgId[] messageId, byte recId, ushort tune_Id)
        {
            byte[] cmd = ThreadCOM.DecStringToByte(REQUESTSINGLE_CMD);
            byte[] request = new byte[4 + 14 * messageId.Length];
            request[0] = cmd[1];
            request[1] = cmd[0];
            request[2] = (byte)((tune_Id & 0xFF00) >> 8);
            request[3] = (byte)(tune_Id & 0x00FF);
            int count = 0;
            foreach (Msg_Par_List.MsgId tu in messageId)
            {
                if (count >= 10)
                {
                    break;
                }
                byte[] onetune = {(byte)((tu.sourceID & 0xFF00) >> 8), 
                    (byte)(tu.sourceID & 0x00FF),
                    (byte)((tu.channelID & 0xFF00) >> 8),
                    (byte)(tu.channelID & 0x00FF),
                    (byte)((tu.msgType & 0xFF00) >> 8),
                    (byte)(tu.msgType & 0x00FF),
                    tu.sourceNo,
                    tu.channelNo,
                    tu.layer,
                    0x00,           // Unused do MsgId
                    0x00, 0x00,     // mpbHd a Null
                    recId,
                    0x00};          // app_rec_len <- número de 'word's (para portas LAN)

                Array.Copy(onetune, 0, request, 4 + 14 * count, onetune.Length);

                count++;
            }
            return request;
        }

        /* Add a tune info to the arraylist of tunes */
        private void addInfo(Msg_Par_List.MsgId messageId, ushort mpbHd, byte rec_id)
        {
            listTunes.Add(new Tune(messageId, mpbHd, rec_id));
        }
    }
}
