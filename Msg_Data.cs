﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CMSniffer
{
    class Msg_Data : Message
    {

        /* Message Body */
        public byte[] body;
        public string bodyhex;

        /* Data MPBHeagers */

        /* Constructors */
        public Msg_Data(Trans_Hd trans_Hd, MessageType Type)
            : base(trans_Hd, Type)
        {
            /* Message Title */
            listViewTitle = ("Tune Data");
        }

        /* Fills List to be used in ListView with message information */
        public override void fillByByteArray(byte[] raw)
        {
            body = raw;
            bodyhex = "";

            /* Parse Message Body */
            for (int i = 0; i < raw.Length; i++)
            {
                bodyhex += String.Format("{0,2:X2} ", body[i]);
            }

            bodyhex = bodyhex.Trim();

            /* Pre-compute ListView Item */
            ListViewItem lvi = new ListViewItem(bodyhex);
            listViewItems.Add(lvi);
        }

        /* Prints message in console */
        public override string ToString()
        {
            return HeaderToString("TUNE") +
                String.Format("{0}", bodyhex);
        }

        /* Fill a ListView */
        public override void setToListView(System.Windows.Forms.ListView listview)
        {
            /* Clear previous ListView */
            listview.Clear();

            /* Set new columns' names */
            listview.Columns.Add("Value", -1, HorizontalAlignment.Left);

            /* Add new items to the ListView */
            foreach (ListViewItem lvi in listViewItems)
                listview.Items.Add(lvi);

            listview.Columns[0].Width = -1;
        }
    }
}
